﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private InputWordsView inputWordsView;
    
    [SerializeField] private TimerController timerController;
    //[SerializeField] SessionManagerScriptableObject sessionManager;
    private ISessionManager sessionManager;

    private InputWordsPresenter inputWordsPresenter;

    void Start()
    {
        sessionManager = RemoteSessionManager.Instance;
        ShowInputWords();
    }

    public void ShowInputWords()
    {
        inputWordsPresenter = new InputWordsPresenter(inputWordsView, sessionManager, timerController);
        inputWordsPresenter.OnFinishedTurn += ShowResults;
        inputWordsPresenter.Init();
    }

    private void ShowResults()
    {
        SceneManager.LoadScene("RoundResult");
    } 
}
