using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectCategoriesAndLetterManager : MonoBehaviour
{
    [SerializeField] CategoriesSelectorView categoriesSelectorView;
    [SerializeField] LetterSelectorView letterSelectorView;

    //[SerializeField] SessionManagerScriptableObject sessionManager;
    ISessionManager sessionManager;
    ICategoryRepository categoryRepository;
    ILetterRepository letterRepository;

    CategoriesSelectorPresenter categoriesSelectorPresenter;
    LetterSelectorPresenter LetterSelectorPresenter;


    private void Start() {
        sessionManager = RemoteSessionManager.Instance;
        categoryRepository = new ApiCategoryRepository();
        letterRepository = new ApiLetterRepository();
        categoriesSelectorPresenter = new CategoriesSelectorPresenter(categoriesSelectorView, sessionManager.CurrentSession.CurrentRound, categoryRepository);
        categoriesSelectorPresenter.OnCategoriesConfirmed += OnCategoriesConfirmed;
    }

    private void OnCategoriesConfirmed() {

        LetterSelectorPresenter = new LetterSelectorPresenter(letterSelectorView, sessionManager, letterRepository);
        LetterSelectorPresenter.OnSelectedLetter += DeleyedChangeScene;
    }

    private void DeleyedChangeScene() {
        Invoke(nameof(ChangeScene),3);
    }
    private void ChangeScene() {
        SceneManager.LoadScene("InputWords");
    }
}
