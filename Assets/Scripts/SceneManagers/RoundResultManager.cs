using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RoundResultManager : MonoBehaviour
{
    [SerializeField] RoundResultView view;
    private ISessionManager sessionManager;

    void Start() {
        sessionManager = RemoteSessionManager.Instance;
        RoundResultPresenter presenter = new RoundResultPresenter(view, sessionManager);
        presenter.OnPresenterFinished += GoToMenu;
        presenter.Init();
    }

    public void GoToMenu() {
        SceneManager.LoadScene("MainScreen");
    }

}
