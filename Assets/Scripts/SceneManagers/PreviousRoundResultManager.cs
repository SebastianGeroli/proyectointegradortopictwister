using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PreviousRoundResultManager : MonoBehaviour
{
    [SerializeField] RoundResultView view;
    private ISessionManager sessionManager;

    void Start() {
        sessionManager = RemoteSessionManager.Instance;
        RoundResultPresenter presenter = new PreviousRoundResultPresenter(view, sessionManager);
        presenter.OnPresenterFinished += GoToMenu;
        presenter.Init();
    }

    public void GoToMenu() {
        Round currentRound = sessionManager.CurrentSession.CurrentRound;

        if (currentRound.Categories == null || currentRound.Categories.Count == 0 || currentRound.Letter == null || currentRound.Letter.Char.Trim() == "") {
            SceneManager.LoadScene("SelectCategoriesAndLetterScene");
        } else {
            SceneManager.LoadScene("InputWords");
        }
    }

}
