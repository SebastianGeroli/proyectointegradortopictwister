using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoginManager : MonoBehaviour
{
    //[SerializeField] private SessionManagerScriptableObject sessionManager;
    private ISessionManager sessionManager;
    private IPlayerRepository playerRepository;

    [SerializeField] private LoginView view;
    private LoginPresenter presenter;
    private void Start() {
        playerRepository = new ApiPlayerRepository();
        sessionManager = RemoteSessionManager.Instance;
        presenter = new LoginPresenter(view, sessionManager, playerRepository);
        presenter.OnComplete += GoToMainMenu;
    }

    public void GoToMainMenu() {
        SceneManager.LoadScene("MainScreen");
    }
}
