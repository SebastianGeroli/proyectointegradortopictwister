using System;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
    //[SerializeField] private SessionManagerScriptableObject sessionManager;
    [SerializeField] private SessionsView sessionView;
    private SessionsPresenter presenter;
    private ISessionManager sessionManager;

    void Start() {

        sessionManager = RemoteSessionManager.Instance;

        if (sessionManager.LocalPlayer == null) {
            sessionManager.Login(PlayerManager.CurrentPlayer);
        }

        //sessionManager.GetActiveSessions.ForEach(PlayBot);

        presenter = new SessionsPresenter(sessionView, sessionManager, 3);
        presenter.OnActiveSessionSelected += PlaySession;
    }

    [Obsolete]
    private void PlayBot(Session session) {
        if (session.CurrentRound.CurrentPlayer.ID == "IBot") {
            CategoryManager categoryManager = new CategoryManager(new LocalCategoryRepository());
            categoryManager.GetRandomCategories(3, (categories) => {
                session.CurrentRound.Categories = categories;
                LetterManager letterManager = new LetterManager(new LocalLetterRepository());
                letterManager.GetRandomLetter(letter => {
                    session.CurrentRound.Letter = letter;
                    BotManager.CompleteTurn(session.CurrentRound, session.CurrentRound.CurrentTurn, (BotPlayer)session.CurrentRound.CurrentPlayer);
                });
            });
        }
    }

    public void PlaySession(Session session) {
        if(session.PreviousRound != null) {
            UnityEngine.SceneManagement.SceneManager.LoadScene("PreviousRoundResult");
        } else if (session.CurrentRound.Categories == null || session.CurrentRound.Categories.Count == 0 || session.CurrentRound.Letter == null || session.CurrentRound.Letter.Char.Trim() == "") {
            UnityEngine.SceneManagement.SceneManager.LoadScene("SelectCategoriesAndLetterScene");
        } else {
            UnityEngine.SceneManagement.SceneManager.LoadScene("InputWords");
        }
    }
}
