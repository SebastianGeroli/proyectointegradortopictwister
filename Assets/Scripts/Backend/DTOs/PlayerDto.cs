using System;

namespace DTOs
{
    [Serializable]
    public class PlayerDto
    {
        public string playerID;
        public string playerName;
        public string password;

        public PlayerDto(string playerID, string playerName, string password) {
            this.playerID = playerID;
            this.playerName = playerName;
            this.password = password;
        }
    }
}