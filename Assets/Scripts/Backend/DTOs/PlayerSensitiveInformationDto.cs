using System;

namespace DTOs
{
    [Serializable]
    public class PlayerSensitiveInformationDto
    {
        public string username;
        public string password;
    }
}