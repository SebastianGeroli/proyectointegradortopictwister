using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace DTOs
{
    [Serializable]
    public class SessionResultDTO
    {
        public string sessionID;
        public string player1Name;
        public string player2Name;
        public int player1Result;
        public int player2Result;
        public bool isPlayer1Winner;
        public bool isPlayer2Winner;
    }
}