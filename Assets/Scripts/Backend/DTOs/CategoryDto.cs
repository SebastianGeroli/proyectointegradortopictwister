using System;

namespace DTOs
{
    [Serializable]
    public class CategoryDto
    {
        public string categoryID;
        public string categoryName;
    }
}