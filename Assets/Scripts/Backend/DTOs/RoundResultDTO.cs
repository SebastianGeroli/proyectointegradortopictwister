using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class RoundResultDTO
{
    public string player1Name;
    public string player2Name;
    public List<RoundResultByCategoryDTO> roundResults;
    public string letter;

    public bool isPlayer1Winner;
    public bool isPlayer2Winner;
    public float player1Time;
    public float player2Time;

    public RoundResultDTO(Round round) {

        player1Name = round.Turn1.Player.Name;
        player2Name = round.Turn2.Player.Name;

        List<Player> winners = new List<Player>(round.Winner);

        isPlayer1Winner = winners.Contains(round.Turn1.Player);
        isPlayer2Winner = winners.Contains(round.Turn2.Player);

        roundResults = new List<RoundResultByCategoryDTO>();
        foreach (var category in round.Categories) {
            RoundResultByCategoryDTO roundResultByCategoryDTO = new RoundResultByCategoryDTO();
            roundResultByCategoryDTO.category = category.Name;
            roundResults.Add(roundResultByCategoryDTO);
            foreach (var answer in round.Turn1.Answers)
            {
                if (answer.Category==category) {
                    roundResultByCategoryDTO.wordPlayer1 = answer.Word;
                    roundResultByCategoryDTO.isCorrectPlayer1 = answer.IsCorrect;
                }
                
            }
            foreach (var answer in round.Turn2.Answers)
            {
                if (answer.Category == category)
                {
                    roundResultByCategoryDTO.wordPlayer2 = answer.Word;
                    roundResultByCategoryDTO.isCorrectPlayer2 = answer.IsCorrect;
                }

            }
        }

        letter = round.Letter.Char;
    }

}
