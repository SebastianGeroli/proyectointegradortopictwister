using System;
using System.Collections.Generic;

namespace DTOs
{
    [Serializable]
    public class RoundDto
    {
        public string roundID;
        public string sessionID;
        public string letterID;
        public List<TurnDto> turns;
        public bool finished;
        public List<PlayerDto> winners;
        public PlayerDto currentPlayer;
        public TurnDto currentTurn;
        public List<CategoryDto> categories;
        public LetterDto letter;
    }
}