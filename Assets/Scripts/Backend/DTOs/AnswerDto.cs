using System;

namespace DTOs
{
    [Serializable]
    public class AnswerDto
    {
        public string answerID;
        public string wordAnswered;
        public string wordID;
        public string categoryID;
        public string turnID;
        public bool correct;
    }
}