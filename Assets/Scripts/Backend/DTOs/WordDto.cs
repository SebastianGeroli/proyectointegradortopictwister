using System;

namespace DTOs
{
    [Serializable]
    public class WordDto
    {
        public string wordID;
        public string wordName;
        public string letterID;
    }
}