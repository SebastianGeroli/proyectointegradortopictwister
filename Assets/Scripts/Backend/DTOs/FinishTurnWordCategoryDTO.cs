﻿using System;

namespace Backend.APIs.AsyncAPIs
{
    [Serializable]
    public class FinishTurnWordCategoryDTO
    {
        public string word;
        public string categoryID;

        public FinishTurnWordCategoryDTO(string word, string categoryID) {
            this.word = word;
            this.categoryID = categoryID;
        }
    }
}