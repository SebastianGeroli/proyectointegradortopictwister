using System;

namespace DTOs
{
    [Serializable]
    public class RoundCategoryDto
    {
        public string roundCategoryID;
        public string categoryID;
        public string roundID;
    }
}