using System;
using System.Collections.Generic;

namespace DTOs
{
    [Serializable]
    public class TurnDto
    {
        public string turnID;
        public string playerID;
        public List<AnswerDto> answers;
        public bool finished;
        public float finishTime;
        public int correntAnswers;
    }
}