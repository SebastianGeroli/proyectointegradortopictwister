using System;

namespace DTOs
{
    [Serializable]
    public class ServerResponseBaseDto<T>
    {
        public string dtoName;
        public T dto;
        public int responseCode;
        public string responseMessage;
    }
}