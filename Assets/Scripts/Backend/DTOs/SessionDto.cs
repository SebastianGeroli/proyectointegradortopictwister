using System;
using System.Collections.Generic;

namespace DTOs
{
    [Serializable]
    public class SessionDto
    {
        public string sessionID;
        public List<RoundDto> rounds;
        public RoundDto currentRound;
        public bool finished;
    }
}