using System;

namespace DTOs
{
    [Serializable]
    public class PlayerSessionDto
    {
        public string playerSessionID;
        public string playerID;
        public string sessionID;
    }
}