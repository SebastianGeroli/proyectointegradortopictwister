﻿using System;

[Serializable]
public class RoundResultByCategoryDTO
{
    public string category;
    public string wordPlayer1;
    public string wordPlayer2;
    public bool isCorrectPlayer1;
    public bool isCorrectPlayer2;
}
