﻿using System;

[Serializable]
public class ActiveSessionDTO
{
    public string sessionID;
    public int currentRoundNumber;
    public int roundsCount;
    public string opponentName;
    public bool isLocalPlayer;

    public ActiveSessionDTO() {

    }

    public ActiveSessionDTO( Session session, Player localPlayer) {
        sessionID = session.ID;
        currentRoundNumber = Array.IndexOf(session.Rounds, session.CurrentRound) + 1;
        roundsCount = session.Rounds.Length;
        opponentName = session.GetOpponent(localPlayer)?.Name;// (session.CurrentRound.Turn1.Player == localPlayer ? session.CurrentRound.Turn2.Player : session.CurrentRound.Turn1.Player).Name;
        isLocalPlayer = session.CurrentRound.CurrentPlayer == localPlayer;
    }
}