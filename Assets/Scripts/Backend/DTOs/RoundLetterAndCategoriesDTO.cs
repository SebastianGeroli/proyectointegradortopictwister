﻿using System;
using System.Collections.Generic;

namespace Backend.APIs.AsyncAPIs
{
    [Serializable]
    public class RoundLetterAndCategoriesDTO
    {
        public string letterID;
        public List<string> categoriesIDs;

        public RoundLetterAndCategoriesDTO(string letterID, List<string> categoriesIDs) {
            this.letterID = letterID;
            this.categoriesIDs = categoriesIDs;
        }
    }
}