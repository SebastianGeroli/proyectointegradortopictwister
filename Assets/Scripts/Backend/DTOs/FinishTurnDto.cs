﻿using System;
using System.Collections.Generic;

namespace Backend.APIs.AsyncAPIs
{
    [Serializable]
    public class FinishTurnDto
    {
        public float time;
        public List<FinishTurnWordCategoryDTO> wordCategories;

        public FinishTurnDto(float time, List<string> words, List<string> categoriesIDs) {
            this.time = time;
            wordCategories = new List<FinishTurnWordCategoryDTO>();
            for (int i = 0; i < categoriesIDs.Count; i++) {
                wordCategories.Add(new FinishTurnWordCategoryDTO(words[i], categoriesIDs[i]));
            }
        }
    }
}