using System;

namespace DTOs
{
    [Serializable]
    public class LetterDto
    {
        public string letterID;
        public string letterName;
    }
}