using System;

namespace DTOs
{
    [Serializable]
    public class WordCategoryDto
    {
        public string wordCategoryID;
        public string categoryID;
        public string wordID;
    }
}