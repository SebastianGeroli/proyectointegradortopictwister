using System;
using Backend.Enums;

namespace DTOs
{
    [Serializable]
    public class FinishedSessionDto
    {
        public string sessionID;
        public string opponentID;
        public string opponentName;
        public SessionResult playerLocalStatus;
        public bool needShowResult;
    }
}