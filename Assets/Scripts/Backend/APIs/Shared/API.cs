using System;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace Backend.APIs.Shared
{
    public abstract class API
    {
        protected const string
            //BaseUrl = "https://localhost:5001/";
            BaseUrl = "http://jsmartin-001-site1.dtempurl.com/"; //TODO complete this url when the url base has been defined
            //BaseUrl = "https://039e29ad3845.ngrok.io/";

        protected async Task<TResultType> Get<TResultType>(string url) {
            using var www = UnityWebRequest.Get(url);
            www.SetRequestHeader("Content-Type", "application/json");
            Debug.Log("GET " + url);
            var operation = www.SendWebRequest();

            while (!operation.isDone) {
                await Task.Yield();
            }

            if (www.result != UnityWebRequest.Result.Success) {
                Debug.LogError($"Failed: {www.error}");
                return default;
            } else {
                try {
                    var result = JsonUtility.FromJson<TResultType>(www.downloadHandler.text);
                    Debug.Log($"Success: {www.downloadHandler.text}");
                    return result;
                } catch (Exception e) {
                    Debug.LogError($"{this} could not parse response: {www.downloadHandler.text}. {e.Message}");
                    return default;
                }
            }
        }

        protected async Task<TResultType> Post<TResultType>(string url, string json) {

            using UnityWebRequest www = new UnityWebRequest(url, "POST");

            byte[] raw = Encoding.UTF8.GetBytes(json);
            www.uploadHandler = new UploadHandlerRaw(raw);
            www.downloadHandler = new DownloadHandlerBuffer();

            www.SetRequestHeader("Content-Type", "application/json");

            Debug.Log("POST " + www.url + "\n" +json);
            var operation = www.SendWebRequest();

            while (!operation.isDone) {
                await Task.Yield();
            }

            if (www.result != UnityWebRequest.Result.Success) {
                Debug.LogError($"Failed: {www.error}");
                return default;
            } else {
                try {
                    var result = JsonUtility.FromJson<TResultType>(www.downloadHandler.text);
                    Debug.Log($"Success: {www.downloadHandler.text}");
                    return result;
                } catch (Exception e) {
                    Debug.LogError($"{this} could not parse response: {www.downloadHandler.text}. {e.Message}");
                    return default;
                }
            }
        }
    }
}