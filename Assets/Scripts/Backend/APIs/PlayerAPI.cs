using System;
using System.Collections;
using System.Collections.Generic;
using Backend.APIs.Shared;
using DTOs;
using UnityEngine;
using UnityEngine.Networking;

namespace Backend.APIs
{
    public class PlayerAPI : API
    {
        public IEnumerator CreatePlayer(string name)
        {
            const string resource = "CreatePlayer";
            var webRequest = UnityWebRequest.Get($"{BaseUrl}{resource}"+ "?playerName="+name);

            yield return webRequest.SendWebRequest();

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                    Debug.LogError($"Couldn't connect!");
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError($"A Protocol error happened!, {webRequest.error}");
                    break;
                case UnityWebRequest.Result.Success:
                    Debug.Log($"Successfully created the player with name: {name}!");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public IEnumerator GetPlayer(string playerID, Action<PlayerDto> onPlayerObtained)
        {
            var resource = $"Player/{playerID}";
            var webRequest = UnityWebRequest.Get($"{BaseUrl}{resource}");

            yield return webRequest.SendWebRequest();

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                    Debug.LogError($"Couldn't connect!");
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError($"A Protocol error happened!, {webRequest.error}");
                    break;
                case UnityWebRequest.Result.Success:
                    var response =
                        JsonUtility.FromJson<ServerResponseBaseDto<PlayerDto>>(webRequest.downloadHandler.text);
                    onPlayerObtained(response.dto);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public IEnumerator LoginPlayer(PlayerSensitiveInformationDto sensitiveInformationDto,
            Action<PlayerDto> onPlayerObtained)
        {
            var resource = $"Player";
            var postData = JsonUtility.ToJson(sensitiveInformationDto);

            var webRequest = UnityWebRequest.Post($"{BaseUrl}{resource}", postData);

            yield return webRequest.SendWebRequest();

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                    Debug.LogError($"Couldn't connect!");
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError($"A Protocol error happened!, {webRequest.error}");
                    break;
                case UnityWebRequest.Result.Success:
                    var response =
                        JsonUtility.FromJson<ServerResponseBaseDto<PlayerDto>>(webRequest.downloadHandler.text);
                    Debug.Log($"Logged in success, loggead as: {response.dto.playerName}");
                    onPlayerObtained(response.dto);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}