using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.APIs.Shared;
using DTOs;
using UnityEngine;

namespace Backend.APIs.AsyncAPIs
{
    public class RoundAsyncAPI : API
    {
        public async Task<RoundDto> GetRound(string roundID)
        {
            const string resource = "Round/";
            var result = await Get<ServerResponseBaseDto<RoundDto>>($"{BaseUrl}{resource}{roundID}");
            return result.dto;
        }
        
        public async Task<List<RoundDto>> GetRounds(string sessionID)
        {
            const string resource = "Rounds/";
            var result = await Get<ServerResponseBaseDto<List<RoundDto>>>($"{BaseUrl}{resource}{sessionID}");
            return result.dto;
        }

        public async Task SetLetterAndCategories(string roundID, string letterID, List<string> categoriesIDs) {
            const string resource = "round/";     
            var result = await Post<ServerResponseBaseDto<string>>($"{BaseUrl}{resource}{roundID}/LetterAndCategories", JsonUtility.ToJson(new RoundLetterAndCategoriesDTO(letterID, categoriesIDs)));
            //return result.dto;
        }

        public async Task<RoundResultDTO> GetRoundResult(string roundID) {
            string resource = $"round/{roundID}/result";
            var result = await Get<ServerResponseBaseDto<RoundResultDTO>>($"{BaseUrl}{resource}");
            return result.dto;
        }
    }
}