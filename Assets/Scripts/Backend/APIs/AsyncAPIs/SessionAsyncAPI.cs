using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.APIs.Shared;
using DTOs;

namespace Backend.APIs.AsyncAPIs
{
    public class SessionAsyncAPI : API
    {
        public const string resource = "session/";
        public async Task<SessionDto> GetNewSession(string localPlayerID, string opponentPlayerID) {
            var result = await Get<ServerResponseBaseDto<SessionDto>>($"{BaseUrl}{resource}{localPlayerID},{opponentPlayerID}");
            return result.dto;
        }

        public async Task<SessionDto> GetSession(string sessionID)
        {
            var result = await Get<ServerResponseBaseDto<SessionDto>>($"{BaseUrl}{resource}{sessionID}");
            return result.dto;
        }
        
        public async Task<List<SessionDto>> GetSessions(string playerID)
        {
            var result = await Get<ServerResponseBaseDto<List<SessionDto>>>($"{BaseUrl}{resource}{playerID}");
            return result.dto;
        }

        public async Task<List<ActiveSessionDTO>> GetActiveSessions(string playerID) {
            var result = await Get<ServerResponseBaseDto<List<ActiveSessionDTO>>>($"{BaseUrl}sessions/active/{playerID}");
            return result.dto;
        }

        public async Task<List<FinishedSessionDto>> GetFinishedSessions(string playerID)
        {
            var result = await Get<ServerResponseBaseDto<List<FinishedSessionDto>>>($"{BaseUrl}sessions/finished/{playerID}");
            return result.dto;
        }

        public async Task<SessionDto> CreateRematch(string localPlayerID, string opponentID)
        {
            var result = await Get<ServerResponseBaseDto<SessionDto>>($"{BaseUrl}{resource}{localPlayerID},{opponentID}");
            return result.dto;
        }

        internal async Task<SessionResultDTO> GetSessionResult(string sessionID) {
            var result = await Get<ServerResponseBaseDto<SessionResultDTO>>($"{BaseUrl}session/{sessionID}/result");
            return result.dto;
        }
    }
}