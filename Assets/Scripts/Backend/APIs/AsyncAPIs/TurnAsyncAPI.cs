using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.APIs.Shared;
using DTOs;
using UnityEditor;
using UnityEngine;

namespace Backend.APIs.AsyncAPIs
{
    public class TurnAsyncAPI : API
    {
        public async Task<TurnDto> GetTurn(string turnID)
        {
            const string resource = "Turn/";
            var result = await Get<ServerResponseBaseDto<TurnDto>>($"{BaseUrl}{resource}{turnID}");
            return result.dto;
        }

        public async Task<List<TurnDto>> GetTurns(string roundID)
        {
            const string resource = "Turns/";
            var result = await Get<ServerResponseBaseDto<List<TurnDto>>>($"{BaseUrl}{resource}{roundID}");
            return result.dto;
        }

        public async Task<TurnDto> FinishTurn(string turnID, float time, List<string> words, List<string> categoryIDs) {
            string resource = $"turn/{turnID}/finish";
            var result = await Post<ServerResponseBaseDto<TurnDto>>($"{BaseUrl}{resource}", JsonUtility.ToJson(new FinishTurnDto(time, words, categoryIDs)));
            return result.dto;
        }
    }
}