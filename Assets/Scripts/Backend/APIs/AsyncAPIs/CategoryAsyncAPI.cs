using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.APIs.Shared;
using DTOs;
using UnityEngine;
using UnityEngine.Networking;

namespace Backend.APIs.AsyncAPIs
{
    public class CategoryAsyncAPI : API
    {
        public async Task<List<CategoryDto>> GetAll()
        {
            const string resource = "GetAllCategories";
            var result = await Get<ServerResponseBaseDto<List<CategoryDto>>>($"{BaseUrl}{resource}");
            return result.dto;
        }
        
        public async Task<List<CategoryDto>> GetRandomCategories()
        {
            const string resource = "GetRandomCategories";
            var result = await Get<ServerResponseBaseDto<List<CategoryDto>>>($"{BaseUrl}{resource}");
            return result.dto;
        }

        public async Task<CategoryDto> GetCategory(string categoryID)
        {
            const string resource = "Category/";
            var result = await Get<ServerResponseBaseDto<CategoryDto>>($"{BaseUrl}{resource}{categoryID}");
            return result.dto;
        }
    }
}