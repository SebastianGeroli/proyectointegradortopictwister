using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.APIs.Shared;
using DTOs;
using UnityEngine;
using UnityEngine.Networking;

namespace Backend.APIs.AsyncAPIs
{
    public class LetterAsyncAPI : API
    {
        public async Task<LetterDto> GetLetter(string letterID)
        {
            const string resource = "Letter/";
            var result = await Get<ServerResponseBaseDto<LetterDto>>($"{BaseUrl}{resource}{letterID}");
            return result.dto;
        }

        public async Task<LetterDto> GetRandomLetter()
        {
            const string resource = "GetRandomLetter";
            var result = await Get<ServerResponseBaseDto<LetterDto>>($"{BaseUrl}{resource}");
            return result.dto;
        }

        public async Task<List<LetterDto>> GetAllLetters() {
            const string resource = "Letters/";
            var result = await Get<ServerResponseBaseDto<List<LetterDto>>>($"{BaseUrl}{resource}");
            return result.dto;
        }
    }
}