using System.Threading.Tasks;
using Backend.APIs.Shared;
using DTOs;
using UnityEngine;
using UnityEngine.Networking;

namespace Backend.APIs.AsyncAPIs
{
    public class PlayerAsyncAPI : API
    {
        public const string resource = "player/";

        public async Task<PlayerDto> CreatePlayer(string name, string password) {
            var result = await Post<ServerResponseBaseDto<PlayerDto>>($"{BaseUrl}{resource}", JsonUtility.ToJson(new PlayerDto(null, name, password)));
            if (result.responseCode != 0) {
                throw new System.Exception(result.responseMessage);
            }
            return result.dto;
        }

        public async Task<PlayerDto> LoginPlayer(string name, string password) {
            var result = await Post<ServerResponseBaseDto<PlayerDto>>($"{BaseUrl}{resource}login", JsonUtility.ToJson(new PlayerDto(null, name, password)));
            if (result.responseCode != 0) {
                throw new System.Exception(result.responseMessage);
            }
            return result.dto;
        }

        public async Task<PlayerDto> GetPlayer(string playerID) {
            var result = await Get<ServerResponseBaseDto<PlayerDto>>($"{BaseUrl}{resource}{playerID}");
            return result.dto;
        }

        public async Task<PlayerDto> GetOpponent(string playerID) {
            var result = await Get<ServerResponseBaseDto<PlayerDto>>($"{BaseUrl}{resource}{playerID}/opponent");
            if (result.responseCode != 0) {
                throw new System.Exception(result.responseMessage);
            }
            return result.dto;
        }
    }
}