using System.Threading.Tasks;
using Backend.APIs.Shared;
using DTOs;

namespace Backend.APIs.AsyncAPIs
{
    public class WordAsyncAPI : API
    {
        public async Task<WordDto> GetWord(string wordID)
        {
            const string resource = "Word/";
            var result = await Get<ServerResponseBaseDto<WordDto>>($"{BaseUrl}{resource}{wordID}");
            return result.dto;
        }
    }
}