using System;
using System.Collections;
using System.Collections.Generic;
using Backend.APIs.Shared;
using DTOs;
using UnityEngine;
using UnityEngine.Networking;

namespace Backend.APIs
{
    public class WordAPI : API
    {
        public IEnumerator GetWord(string wordID, Action<WordDto> onWordObtained)
        {
            var resource = $"Word/{wordID}";
            var webRequest = UnityWebRequest.Get($"{BaseUrl}{resource}");

            yield return webRequest.SendWebRequest();

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                    Debug.LogError($"Couldn't connect!");
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError($"A Protocol error happened!, {webRequest.error}");
                    break;
                case UnityWebRequest.Result.Success:
                    var response =
                        JsonUtility.FromJson<ServerResponseBaseDto<WordDto>>(webRequest.downloadHandler.text);
                    onWordObtained(response.dto);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}