using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.APIs.Shared;
using DTOs;
using UnityEngine;
using UnityEngine.Networking;

namespace Backend.APIs
{
    public class CategoryAPI : API
    {
        public IEnumerator GetRandomCategories(Action<List<CategoryDto>> onCategoriesObtained)
        {
            const string resource = "GetRandomCategories";
            var webRequest = UnityWebRequest.Get($"{BaseUrl}{resource}");

            yield return webRequest.SendWebRequest();

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                    Debug.LogError($"Couldn't connect!");
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError($"A Protocol error happened!, {webRequest.error}");
                    break;
                case UnityWebRequest.Result.Success:
                    var response =
                        JsonUtility.FromJson<ServerResponseBaseDto<List<CategoryDto>>>(webRequest.downloadHandler.text);
                    onCategoriesObtained(response.dto);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public IEnumerator GetCategory(string categoryID, Action<CategoryDto> onCategoryObtained)
        {
            var resource = $"Category/{categoryID}";
            var webRequest = UnityWebRequest.Get($"{BaseUrl}{resource}");

            yield return webRequest.SendWebRequest();

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                    Debug.LogError($"Couldn't connect!");
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError($"A Protocol error happened!, {webRequest.error}");
                    break;
                case UnityWebRequest.Result.Success:
                    var response =
                        JsonUtility.FromJson<ServerResponseBaseDto<CategoryDto>>(webRequest.downloadHandler.text);
                    onCategoryObtained(response.dto);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}