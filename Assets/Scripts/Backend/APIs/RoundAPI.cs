using System;
using System.Collections;
using System.Collections.Generic;
using Backend.APIs.Shared;
using DTOs;
using UnityEngine;
using UnityEngine.Networking;

namespace Backend.APIs
{
    public class RoundAPI : API
    {
        public IEnumerator GetRound(string roundID, Action<RoundDto> onSessionObtained)
        {
            var resource = $"Round/{roundID}";
            var webRequest = UnityWebRequest.Get($"{BaseUrl}{resource}");

            yield return webRequest.SendWebRequest();

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                    Debug.LogError($"Couldn't connect!");
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError($"A Protocol error happened!, {webRequest.error}");
                    break;
                case UnityWebRequest.Result.Success:
                    var response =
                        JsonUtility.FromJson<ServerResponseBaseDto<RoundDto>>(webRequest.downloadHandler.text);
                    onSessionObtained(response.dto);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public IEnumerator GetRounds(string sessionID, Action<List<RoundDto>> onSessionsObtained)
        {
            var resource = $"Rounds/{sessionID}";
            var webRequest = UnityWebRequest.Get($"{BaseUrl}{resource}");

            yield return webRequest.SendWebRequest();

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                    Debug.LogError($"Couldn't connect!");
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError($"A Protocol error happened!, {webRequest.error}");
                    break;
                case UnityWebRequest.Result.Success:
                    var response =
                        JsonUtility.FromJson<ServerResponseBaseDto<List<RoundDto>>>(webRequest.downloadHandler.text);
                    onSessionsObtained(response.dto);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}