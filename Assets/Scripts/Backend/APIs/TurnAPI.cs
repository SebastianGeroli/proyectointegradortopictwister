using System;
using System.Collections;
using System.Collections.Generic;
using Backend.APIs.Shared;
using DTOs;
using UnityEngine;
using UnityEngine.Networking;

namespace Backend.APIs
{
    public class TurnAPI : API
    {
        public IEnumerator GetTurn(string turnID, Action<TurnDto> onTurnObtained)
        {
            string resource = $"Turn/{turnID}";
            var webRequest = UnityWebRequest.Get($"{BaseUrl}{resource}");

            yield return webRequest.SendWebRequest();

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                    Debug.LogError($"Couldn't connect!");
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError($"A Protocol error happened!, {webRequest.error}");
                    break;
                case UnityWebRequest.Result.Success:
                    var response =
                        JsonUtility.FromJson<ServerResponseBaseDto<TurnDto>>(webRequest.downloadHandler.text);
                    onTurnObtained(response.dto);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public IEnumerator GetTurns(string roundID, Action<List<TurnDto>> onTurnsObtained)
        {
            var resource = $"Turns/{roundID}";
            var webRequest = UnityWebRequest.Get($"{BaseUrl}{resource}");

            yield return webRequest.SendWebRequest();

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                    Debug.LogError($"Couldn't connect!");
                    break;
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError($"A Protocol error happened!, {webRequest.error}");
                    break;
                case UnityWebRequest.Result.Success:
                    var response =
                        JsonUtility.FromJson<ServerResponseBaseDto<List<TurnDto>>>(webRequest.downloadHandler.text);
                    onTurnsObtained(response.dto);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}