namespace Backend.Enums
{
    public enum SessionResult
    {
        Win = 1,
        Lost = 0,
        Tie = 2
    }
}