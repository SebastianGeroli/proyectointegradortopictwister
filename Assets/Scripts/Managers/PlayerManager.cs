using Backend.APIs;
using Backend.APIs.AsyncAPIs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class PlayerManager 
{
    private IPlayerRepository playerRepository;

    private static Player currentPlayer;
    public static Player CurrentPlayer {
        get {
            if (currentPlayer != null)
                return currentPlayer;

            string name = PlayerPrefs.GetString("playerName", "");
            string id = PlayerPrefs.GetString("playerId", "");
            if(id!="")
                currentPlayer = new LocalPlayer(name, id);
            
            return currentPlayer;            
        }
        set {
            currentPlayer = value;
            if (value != null) {
                PlayerPrefs.SetString("playerName", value.Name);
                PlayerPrefs.SetString("playerId", value.ID);
            } else {
                PlayerPrefs.SetString("playerName", "");
                PlayerPrefs.SetString("playerId", "");
            }
        } 
    }

    public PlayerManager(IPlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }
    public async void CreatePlayer(string name, string password, Action<Player> onCreatedCallback, Action<string> onErrorCallback) {

        CurrentPlayer = await playerRepository.CreatePlayer(name, password, onErrorCallback);
        if (currentPlayer != null) {
            onCreatedCallback?.Invoke(currentPlayer);
        } 
    }

    public async void LoginPlayer(string name, string password, Action<Player> onLoginCallback, Action<string> onErrorCallback) {

        CurrentPlayer = await playerRepository.LoginPlayer(name, password, onErrorCallback);
        if (currentPlayer != null) {
            onLoginCallback?.Invoke(currentPlayer);
        }
    }

    public async void GetOpponent(Player currentPlayer, Action<Player> onOpponentFound, Action<string> onErrorCallback) {
        Player opponent = await playerRepository.GetOpponent(currentPlayer, onErrorCallback);

        onOpponentFound?.Invoke(opponent);

    }
}
