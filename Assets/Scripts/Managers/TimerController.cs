using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class TimerController : MonoBehaviour, ITimer
{
    [SerializeField] private TMP_Text currentTimeText;
    private float currentTime;
    private bool isRunning;
    private Action onTimeOverListener;
    private string timeFormat = "0.00";

    public float CurrentTime { get => currentTime; }

    public void StartCountDown(float time, Action onTimeOver)
    {
        currentTime = time;
        currentTimeText.text = currentTime.ToString(timeFormat);
        onTimeOverListener = onTimeOver;
        isRunning = true;
    }

    public void StopCountDown()
    {
        isRunning = false;
    }

    
    void Update()
    {
        if (isRunning)
        {
            currentTime -= Time.deltaTime;
            currentTimeText.text = currentTime.ToString(timeFormat);
            if (currentTime <= 0)
            {
                isRunning = false;
                onTimeOverListener?.Invoke();
            }
        }
    }
}
