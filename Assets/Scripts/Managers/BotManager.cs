using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotManager {

    public static BotPlayer CreatePlayer() {

        BotPlayer botplayer = new BotPlayer("IBot", new GameDictionary());
        botplayer.SuccessProbability = BotPlayer.MinSuccessProbability;

        return botplayer;
    }

    public static void CompleteTurn(Round round, Turn turn, BotPlayer bot) {
        List<Word> words = new List<Word>();
        foreach (var category in round.Categories) {
            float random = UnityEngine.Random.Range(0f, 1f);
            if (bot.PredictSuccess(random)) {
                words.Add(new Word(bot.Answer(category, round.Letter)));
            } else {
                words.Add(new Word(bot.GetRandomWord()));

            }
        }

        turn.FinishTurn(30f, words, round);
    }

}
