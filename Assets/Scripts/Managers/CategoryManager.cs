﻿using System;
using System.Collections.Generic;
using Backend.APIs;

public class CategoryManager
{
    protected List<Category> categoryList = new List<Category>();
    public List<Category> CategoryList => categoryList;//Categories.values;

    private ICategoryRepository categoryRepository;

    public CategoryManager(ICategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public async void GetCategories(Action<List<Category>> OnCategoriesReceivedCallback) {
        var categories = await categoryRepository.GetCategories();
        OnCategoriesReceivedCallback?.Invoke(categories);
    }


    public async void GetRandomCategories(int amountOfCategoriesAskedToReturn, Action<List<Category>> OnCategoriesReceivedCallback) {
        var categories = await categoryRepository.GetRandomCategories(amountOfCategoriesAskedToReturn);
        foreach (var category in categories) {
            if (!categoryList.Contains(category)) {
                categoryList.Add(category);
            }
        }        
        OnCategoriesReceivedCallback?.Invoke(categories);
    }

    public List<Category> GetCategories(List<string> categoriesNames) {
        List<Category> categories = new List<Category>();
        for (int i = 0; i < categoriesNames.Count; i++) {
            foreach (var category in CategoryList) {
                if (category.Name == categoriesNames[i]) {
                    categories.Add(category);
                }
            }
        }
        return categories;
    }

    public static List<string> GetCategoriesNames(List<Category> categories) {
        List<string> categoriesName = new List<string>();
        foreach (Category category in categories) {
            categoriesName.Add(category.Name);
        }
        return categoriesName;
    }
}
