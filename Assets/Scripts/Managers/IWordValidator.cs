﻿public interface IWordValidator
{
    bool ValidateWord(string word, Category category, Letter letter);
}