﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DTOs;

public interface ISessionManager
{
    Player LocalPlayer { get; }
    Session CurrentSession { get; }
    void GetActiveSessions(Action<List<ActiveSessionDTO>> callback, Action<string> onErrorCallback);
    void GetFinishedSessions(Action<List<FinishedSessionDto>> callback,Action<string> onErrorCallback);
    List<Session> FinishedSessions { get; }

    void PlaySession(string sessionID, Action<Session> onSessionReady, Action<string> onErrorCallback);

    void Login(Player player);

    Task StartNewSession(Action<string> OnOpponentFound);
    void GetRoundResult(Round round, Action<RoundResultDTO> onSuccess, Action<string> onErrorCallback);
    void GetSessionResult(string sessionID, Action<DTOs.SessionResultDTO> onSuccess, Action<string> onErrorCallback);
    void SetLetterCurrentRound(Letter letter, Action onSuccess, Action<string> onErrorCallback);
    void FinishTurn(float currentTime, List<Word> words, Round currentRound, Action onSuccess, Action<string> onErrorCallback);
    void PlayRematch(string finishedSessionOpponentID, Action<Session> onSessionReceived, Action<string> onErrorGetSession);
    FinishedSessionDto ShouldShowSomeResult(List<FinishedSessionDto> finishedSessions);
}