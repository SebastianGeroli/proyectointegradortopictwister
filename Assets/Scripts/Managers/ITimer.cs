﻿using System;

public interface ITimer
{
    public float CurrentTime { get ; }

    public void StartCountDown(float time, Action onTimeOver);

    public void StopCountDown();
}