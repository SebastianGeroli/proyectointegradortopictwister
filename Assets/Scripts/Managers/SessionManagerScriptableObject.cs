﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Enums;
using DTOs;
using UnityEngine;


[CreateAssetMenu(fileName = "NewSessionScriptable", menuName = "Scriptables/SessionManager")]
public class SessionManagerScriptableObject : ScriptableObject, ISessionManager
{
    public Session CurrentSession { get; private set; }

    public List<Session> Sessions { get; private set; } = new List<Session>();

   

    public List<Session> FinishedSessions => GetFinishedSessions();

    public Player LocalPlayer { get; protected set; }

    public void AddSession(Session session)
    {
        if (Sessions.Contains(session)) return;

        Sessions.Add(session);
    }

    private List<Session> GetFinishedSessions()
    {
        List<Session> finishedSessions = new List<Session>();

        foreach (var session in Sessions)
        {
            if (session.Finished) finishedSessions.Add(session);
        }

        return finishedSessions;
    }

    private List<Session> GetActiveSessions()
    {
        List<Session> activeSessions = new List<Session>();

        foreach (var session in Sessions)
        {
            if (!session.Finished) activeSessions.Add(session);
        }

        List<Session> orderedActiveSessions = new List<Session>();

        foreach (var session in activeSessions)
        {
            if (session.CurrentRound.CurrentPlayer == LocalPlayer)
                orderedActiveSessions.Add(session);
        }
        foreach (var session in activeSessions)
        {
            if (!orderedActiveSessions.Contains(session)) orderedActiveSessions.Add(session);
        }

        return orderedActiveSessions;
    }

    public void PlaySession(Session session)
    {
        if (session.CurrentRound.CurrentPlayer.ID == LocalPlayer.ID)
        {
            CurrentSession = session;
        }
    }

    public void Login(Player player) {
        Sessions = new List<Session>();
        LocalPlayer = player;
    }

    public async Task StartNewSession(Action<string> OnOpponentFound) {
        Player bot = BotManager.CreatePlayer();
        Session newSession = new LocalSession(LocalPlayer, bot, 3);
        OnOpponentFound?.Invoke(bot.Name);
        AddSession(newSession);
        PlaySession(newSession);
        await Task.CompletedTask;
    }

    public void GetActiveSessions(Action<List<ActiveSessionDTO>> callback, Action<string> onErrorCallback) {
        List<ActiveSessionDTO> dtos = new List<ActiveSessionDTO>();
        Sessions.ForEach(session => {
            dtos.Add(new ActiveSessionDTO(session, LocalPlayer)); ;
        });
        callback(dtos);
    }
    
    public void GetFinishedSessions(Action<List<FinishedSessionDto>> callback, Action<string> onErrorCallback)
    {
        var finishedSessions = new List<FinishedSessionDto>();
        Sessions.ForEach(session =>
        {
            finishedSessions.Add(new FinishedSessionDto()
            {
                opponentName = session.GetOpponent(LocalPlayer).Name,
                opponentID = session.GetOpponent(LocalPlayer).ID,
                playerLocalStatus = SessionResult.Win //Hardcoded -- this should be calculated.
            });
        });
    }

    public void PlaySession(string sessionID, Action<Session> onSessionReady, Action<string> onErrorCallback) {
        CurrentSession = Sessions.Find(session => session.GetHashCode().ToString() == sessionID) ;
    }

    public void SetLetterCurrentRound(Letter letter, Action OnSuccess, Action<string> onErrorCallback) {
        CurrentSession.CurrentRound.Letter = letter;
        OnSuccess?.Invoke();
    }

    public void FinishTurn(float time, List<Word> words, Round round, Action onSuccess, Action<string> onErrorCallback) {
        round.CurrentTurn.FinishTurn(time, words, round);
    }

    public void PlayRematch(string finishedSessionOpponentID, Action<Session> onSessionReceived, Action<string> onErrorGetSession)
    {
        //obsolete class
    }

    public void GetRoundResult(Round round, Action<RoundResultDTO> onSuccess, Action<string> onErrorCallback) {
        onSuccess?.Invoke( new RoundResultDTO(round));
    }

    public void GetSessionResult(string sessionID, Action<DTOs.SessionResultDTO> onSuccess, Action<string> onErrorCallback) {
        throw new NotImplementedException();
    }

    public FinishedSessionDto ShouldShowSomeResult(List<FinishedSessionDto> finishedSessions) {
        throw new NotImplementedException();
    }
}
