using System;
using System.Collections;

public class LetterManager
{
    ILetterRepository letterRepository;

    public LetterManager(ILetterRepository letterRepository)
    {
        this.letterRepository = letterRepository;
    }
    public async void GetRandomLetter(Action<Letter> onLetterReceivedCallback)
    {
        Letter letter = await letterRepository.GetRandomLetter();
        onLetterReceivedCallback?.Invoke(letter);
    }
}
