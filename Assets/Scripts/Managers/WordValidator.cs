using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordValidator : IWordValidator
{
    private GameDictionary gameDictionary;

    public WordValidator(GameDictionary gameDictionary) {
        this.gameDictionary = gameDictionary;
    }

    private bool ValidateWordInCategory(string word, Category category) {
        if (string.IsNullOrEmpty(word))
            return false;

        try {
            if (gameDictionary.FindWord(word).Categories.Contains(category))
                return true;
        } catch (Exception) {
            return false;
        }
        return false;
    }

    private bool ValidateFirstLetterInWord(string word, string letter) {
        string wordLowerCase = word.Trim().ToLower();
        if (wordLowerCase.StartsWith(letter.ToLower())) return true;
        return false;
    }

    public bool ValidateWord(string word, Category category, Letter letter) {
        return ValidateWordInCategory(word, category) && ValidateFirstLetterInWord(word, letter.Char);
    }
}
