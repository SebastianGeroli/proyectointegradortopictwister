using Backend.APIs.AsyncAPIs;
using DTOs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class RemoteSessionManager : ISessionManager
{

    private ISessionRepository sessionRepository;

    private static RemoteSessionManager instance;
    public static RemoteSessionManager Instance {
        get {
            if (instance == null) instance = new RemoteSessionManager(new ApiSessionRepository());
            return instance;
        }
    }

    public RemoteSessionManager(ISessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    public Player LocalPlayer { get; protected set; }

    public Session CurrentSession { get; private set; }

    public List<Session> FinishedSessions => new List<Session>();

    public void Login(Player player) {
        LocalPlayer = player;
    }

    private static List<string> sessionsIDsOfShowedResults = null;
    private List<string> SessionsIDsOfShowedResults {
        get {
            if (sessionsIDsOfShowedResults == null) {
                if (PlayerPrefs.HasKey("sessionsIDsOfShowedResults" + LocalPlayer.ID)) {
                    string recoveredString = PlayerPrefs.GetString("sessionsIDsOfShowedResults" + LocalPlayer.ID);
                    sessionsIDsOfShowedResults = new List<string>(recoveredString.Split(','));
                } else {
                    return null;
                }
            }
            return sessionsIDsOfShowedResults;
        }
        set {
            sessionsIDsOfShowedResults = value;
            PlayerPrefs.SetString("sessionsIDsOfShowedResults" + LocalPlayer.ID, string.Join(",", sessionsIDsOfShowedResults.ToArray()));
        }
    }

    public async void PlaySession(string sessionID, Action<Session> onSessionReady, Action<string> onErrorCallback) {
        CurrentSession = await sessionRepository.GetSession(sessionID, onErrorCallback);
        onSessionReady?.Invoke(CurrentSession);
    }

    public async Task StartNewSession(Action<string> OnOpponentFound) {
        PlayerAsyncAPI playerApi = new PlayerAsyncAPI();
        var opponent = await playerApi.GetOpponent(LocalPlayer.ID);
        OnOpponentFound?.Invoke(opponent.playerName);

        SessionAsyncAPI api = new SessionAsyncAPI();
        SessionDto session = await api.GetNewSession(LocalPlayer.ID, opponent.playerID);

        CurrentSession = new RemoteSession(session);
    }

    public async void GetActiveSessions(Action<List<ActiveSessionDTO>> onActiveSessionsFound, Action<string> onErrorCallback) {
        List<ActiveSessionDTO> activeSessions = await sessionRepository.GetActiveSessions(LocalPlayer.ID, onErrorCallback);
        onActiveSessionsFound(activeSessions);
    }
    public async void GetFinishedSessions(Action<List<FinishedSessionDto>> callback, Action<string> onErrorCallback) {
        var finishedSessions = await sessionRepository.GetFinishedSessions(LocalPlayer.ID, onErrorCallback);
        callback(finishedSessions);
    }

    public async void SetLetterCurrentRound(Letter letter, Action onSuccess, Action<string> onErrorCallback) {
        CurrentSession.CurrentRound.Letter = letter;
        List<string> categoriesIDs = new List<string>();

        CurrentSession.CurrentRound.Categories.ForEach(cat => {
            categoriesIDs.Add(cat.ID);
        });

        await sessionRepository.SetLetterAndCategories(CurrentSession.CurrentRound.ID, letter.ID, categoriesIDs, onErrorCallback);
        onSuccess?.Invoke();
    }

    public async void FinishTurn(float time, List<Word> words, Round currentRound, Action onSuccess, Action<string> onErrorCallback) {
        Turn currentTurn = currentRound.CurrentTurn;
        currentTurn.FinishTurn(time, words, currentRound);

        List<string> wordsAsString = new List<string>();
        words.ForEach(word => {
            wordsAsString.Add(word.Name);
        });

        List<string> categoriesIDs = new List<string>();
        currentRound.Categories.ForEach(cat => {
            categoriesIDs.Add(cat.ID);
        });

        await sessionRepository.FinishTurn(currentTurn.ID, time, wordsAsString, categoriesIDs, onErrorCallback);
        onSuccess?.Invoke();
    }

    public async void PlayRematch(string opponentID, Action<Session> onSessionReceived, Action<string> onErrorGetSession) {
        var session = await sessionRepository.CreateRematch(LocalPlayer.ID, opponentID, onErrorGetSession);
        CurrentSession = session;
        onSessionReceived(session);
    }

    public async void GetRoundResult(Round round, Action<RoundResultDTO> onSuccess, Action<string> onErrorCallback) {
        RoundResultDTO roundResult = await sessionRepository.GetRoundResult(round.ID, onErrorCallback);
        onSuccess(roundResult);
    }

    public async void GetSessionResult(string sessionID, Action<SessionResultDTO> onSuccess, Action<string> onErrorCallback) {
        SessionResultDTO sessionResult = await sessionRepository.GetSessionResult(sessionID, onErrorCallback);
        onSuccess(sessionResult);
    }

    public FinishedSessionDto ShouldShowSomeResult(List<FinishedSessionDto> finishedSessions) {
        var ids = SessionsIDsOfShowedResults;
        if(ids == null) {
            ids = new List<string>();
            finishedSessions.ForEach(session => ids.Add(session.sessionID));
            SessionsIDsOfShowedResults = ids;
        }

        foreach (var session in finishedSessions) {
            if (!ids.Contains(session.sessionID)) {
                ids.Add(session.sessionID);
                SessionsIDsOfShowedResults = ids;
                return session;
            }
        }

        return null;
    }
}
