using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class ActiveSessionRowView : MonoBehaviour
{
    [SerializeField] private Button button;
    [SerializeField] private TextMeshProUGUI informationText;
    [SerializeField] private GameObject waitingObject;
    private ActiveSessionDTO session;

    private event Action<ActiveSessionDTO> OnPlaySession;

    public void Initialize(ActiveSessionDTO session, Action<ActiveSessionDTO> OnPlaySession)
    {
        this.session = session;
        this.OnPlaySession += OnPlaySession;
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(PlaySession);
        SetInformation();
        ShowButtonPlay();
    }

    public void PlaySession()
    {
        OnPlaySession?.Invoke(session);
    }

    private void SetInformation()
    {
        informationText.text = $"{session.opponentName} \nRonda: {session.currentRoundNumber}/{session.roundsCount}";
    }

    private void ShowButtonPlay()
    {
        waitingObject.SetActive(!session.isLocalPlayer);
        button.gameObject.SetActive(session.isLocalPlayer);
    }
}
