using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class CategoryRoundResultUI : MonoBehaviour
{
    [SerializeField] TMP_Text categoryTxt;
    [SerializeField] TMP_Text wordPlayer1Txt;
    [SerializeField] TMP_Text wordPlayer2Txt;
    [SerializeField] Toggle correctPlayer1;
    [SerializeField] Toggle correctPlayer2;

    public void Init(RoundResultByCategoryDTO dto) {
        categoryTxt.text = dto.category;
        wordPlayer1Txt.text = dto.wordPlayer1;
        wordPlayer2Txt.text = dto.wordPlayer2;
        correctPlayer1.gameObject.SetActive(!string.IsNullOrEmpty(dto.wordPlayer1));
        correctPlayer2.gameObject.SetActive(!string.IsNullOrEmpty(dto.wordPlayer2));
        correctPlayer1.isOn = dto.isCorrectPlayer1;
        correctPlayer2.isOn = dto.isCorrectPlayer2;
    }
}
