﻿using TMPro;
using UnityEngine;

public class CategorySelectorUI : MonoBehaviour
{
    [SerializeField] private TMP_Text categoryNamaText;
    public void Init(string categoryName) {
        categoryNamaText.text = categoryName;
    }
}