using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UserPanelView : MonoBehaviour
{
    [SerializeField] TMP_Text playerNameLabel;
    public void Init(string playerName) {
        playerNameLabel.text = playerName;
    }
}
