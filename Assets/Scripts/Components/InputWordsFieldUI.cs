using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InputWordsFieldUI : MonoBehaviour
{
    [SerializeField] private TMP_InputField wordInputField;
    [SerializeField] private TMP_Text categoryText;

    public delegate string OnWordChangedDelegate(string text, int index);
    private event OnWordChangedDelegate onWordChangedListener;

    private int index;
    private string category;

    public TMP_Text CategoryText => categoryText;

    public string Category
    {
        get => category;
        set { category = value; categoryText.text = category; }
    }

    public void Init(string category, string word, int index, OnWordChangedDelegate onWordChanged)
    {
        onWordChangedListener += onWordChanged;
        this.index = index;
        Category = category;
        wordInputField.text = word;
        wordInputField.onEndEdit.AddListener(OnWordChanged);
    }

    private void OnWordChanged(string text)
    {
        wordInputField.text = onWordChangedListener.Invoke(text, index);
    }

    public void DisableInput() {
        wordInputField.interactable = false;
    }
}
