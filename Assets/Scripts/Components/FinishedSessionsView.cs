using System;
using Backend.Enums;
using DTOs;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Presenters
{
    public class FinishedSessionsView : MonoBehaviour
    {
        [SerializeField] private Button button;
        [SerializeField] private Button buttonView;
        [SerializeField] private TextMeshProUGUI opponentNameText;
        [SerializeField] private TextMeshProUGUI roundResultText;
        private FinishedSessionDto finishedSessionDto;

        private event Action<FinishedSessionDto> OnRematchClicked;
        private event Action<FinishedSessionDto> OnViewResultClicked;
        public void Initialize(FinishedSessionDto finishedSessionDto, Action<FinishedSessionDto> OnRematchClicked, Action<FinishedSessionDto> OnViewResultClicked) {
            this.finishedSessionDto = finishedSessionDto;
            this.OnRematchClicked += OnRematchClicked;
            this.OnViewResultClicked += OnViewResultClicked;
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(Rematch);
            buttonView.onClick.RemoveAllListeners();
            buttonView.onClick.AddListener(ShowResult);
            SetInformation();
        }

        private void SetInformation() {
            opponentNameText.text = finishedSessionDto.opponentName;

            if (finishedSessionDto.needShowResult) {
                roundResultText.gameObject.SetActive(false);
            } else {
                roundResultText.gameObject.SetActive(true);
                roundResultText.text = GetText(finishedSessionDto.playerLocalStatus);
            }

        }

        private string GetText(SessionResult playerLocalStatus) {
            switch (playerLocalStatus) {
                case SessionResult.Win:
                    return "Ganaste!";
                case SessionResult.Lost:
                    return "Perdiste";
                case SessionResult.Tie:
                    return "Empate!";
                default:
                    throw new ArgumentOutOfRangeException(nameof(playerLocalStatus), playerLocalStatus, null);
            }
        }

        private void Rematch() {
            OnRematchClicked?.Invoke(finishedSessionDto);
        }

        private void ShowResult() {
            OnViewResultClicked?.Invoke(finishedSessionDto);
        }
    }
}