using DTOs;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SessionResultUI : MonoBehaviour
{
    [SerializeField] TMP_Text player1NameText;
    [SerializeField] TMP_Text player1ResultText;
    [SerializeField] TMP_Text player2NameText;
    [SerializeField] TMP_Text player2ResultText;
    [SerializeField] GameObject player1IsWinGameObject;
    [SerializeField] GameObject player2IsWinGameObject;


    public void Init(SessionResultDTO result) {
        player1NameText.text = result.player1Name;
        player2NameText.text = result.player2Name;

        player1ResultText.text = result.player1Result.ToString();
        player2ResultText.text = result.player2Result.ToString();

        player1IsWinGameObject.SetActive(result.isPlayer1Winner);
        player2IsWinGameObject.SetActive(result.isPlayer2Winner);

        gameObject.SetActive(true);

    }

    public void Close() { gameObject.SetActive(false); }
}
