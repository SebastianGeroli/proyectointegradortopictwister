﻿public interface IRoundResultView
{
    public void Init( RoundResultPresenter presenter);
    public void ShowResult(RoundResultDTO roundResult);
    public void ShowTitle(string title);

}