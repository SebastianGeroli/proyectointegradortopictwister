using System.Collections;
using System.Collections.Generic;
using DTOs;
using UnityEngine;

public interface ISessionsView
{
    void Init(SessionsPresenter presenter);
    void ShowPlayer(string name);
    void ShowActiveSessions(List<ActiveSessionDTO> activeSessions);
    void ShowOpponent(string name);
    void ShowFinishedSessions(List<FinishedSessionDto> finishedSessions);

    void ShowSessionResult(SessionResultDTO sessionResult);
}
