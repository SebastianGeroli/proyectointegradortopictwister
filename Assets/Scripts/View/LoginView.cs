using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LoginView : MonoBehaviour, ILoginView
{
    [SerializeField] private TMP_Text errorText;
    [SerializeField] private TMP_Text confirmPasswordLabel;
    [SerializeField] private TMP_Text buttonText;
    [SerializeField] private TMP_InputField nameInputField;
    [SerializeField] private TMP_InputField passwordInputField;
    [SerializeField] private TMP_InputField confirmPasswordInputField;
    [SerializeField] private Button loginButton;
    [SerializeField] private Toggle loginToggle;

    LoginPresenter loginPresenter;

    public void Init(LoginPresenter loginPresenter)
    {
        this.loginPresenter = loginPresenter;
                
        HideErrorMessage();

        loginButton.onClick.AddListener(OnButtonClick);

        nameInputField.onValueChanged.AddListener((string text) => { HideErrorMessage(); });

        passwordInputField.contentType = TMP_InputField.ContentType.Password;
        passwordInputField.text = "";

        confirmPasswordInputField.contentType = TMP_InputField.ContentType.Password;
        confirmPasswordInputField.text = "";

        loginToggle.onValueChanged.RemoveAllListeners();
        loginToggle.onValueChanged.AddListener(OnLoginToggleChanged);

    }

    public void ShowPlayerName(string name) {
        if (string.IsNullOrEmpty(name)) {
            nameInputField.text = "";
            loginToggle.isOn = false;
        } else {
            nameInputField.text = name;
            loginToggle.isOn = true;
        }
    }

    private void OnButtonClick()
    {
        string name = nameInputField.text;
        string password = passwordInputField.text;
        string confirmPassword = confirmPasswordInputField.text;
        if (loginToggle.isOn) {
            loginPresenter.Login(name, password);
        } else {
            loginPresenter.CreatePlayer(name, password,confirmPassword);
        }
    }

    public void ShowErrorMessage(string message)
    {
        errorText.text = message;
    }

    private void HideErrorMessage()
    {
        errorText.text = "";
    }

    private void OnLoginToggleChanged(bool isOn) {
        confirmPasswordLabel.gameObject.SetActive(!isOn);
        confirmPasswordInputField.gameObject.SetActive(!isOn);
        buttonText.text = isOn ? "Ingresar" : "Registrar";
    }
}
