using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LetterSelectorView : MonoBehaviour, ILetterSelectorView
{
    [SerializeField] public TMP_Text letterText;
    [SerializeField] public GameObject letterContainer;
    [SerializeField] public Button selectLetterButton;

    private LetterSelectorPresenter presenter;

    private void Start() {
        selectLetterButton.gameObject.SetActive(false);
        letterContainer.gameObject.SetActive(false);
    }

    public void Init(LetterSelectorPresenter presenter) {
        selectLetterButton.gameObject.SetActive(true);
        this.presenter = presenter;
        selectLetterButton.onClick.RemoveAllListeners();
        selectLetterButton.onClick.AddListener(presenter.SelectRandomLetter) ;
    }

    public void ShowLetter(string letter) {
        letterText.text = $"Letra: {letter}";
        letterText.gameObject.SetActive(true);
        letterContainer.SetActive(true);
        selectLetterButton.gameObject.SetActive(false);
    }
}
