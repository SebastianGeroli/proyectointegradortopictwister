using System.Linq;
using System.Collections.Generic;
using DTOs;
using Presenters;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SessionsView : MonoBehaviour, ISessionsView
{
    [SerializeField] private ActiveSessionRowView ActiveSessionsRowPrefab;
    [SerializeField] private FinishedSessionsView FinishedSessionRowPrefab;
    [SerializeField] private UserPanelView UserPanel;
    [SerializeField] private Transform activeSessionContainer;
    [SerializeField] private Transform finishedSessionContainer;
    [SerializeField] private Button startNewSessionButton;
    [SerializeField] private Button RefreshActiveSessionsButton;
    [SerializeField] private Button RefreshFinishedSessionsButton;
    [SerializeField] private GameObject OpponentPopup;
    [SerializeField] private TMP_Text OpponentPopupName;
    [SerializeField] private GameObject transitionScreen;
    [SerializeField] private SessionResultUI sessionResultUI;
    [SerializeField] private GameObject finishSessionsTitle;
    [SerializeField] private GameObject finishSessionsFooter;
    [SerializeField] private GameObject activeSessionsTitle;
    [SerializeField] private GameObject activeSessionsFooter;


    private SessionsPresenter presenter;
    private bool activeSessionsDone = false;
    private bool finishedSessionsDone = false;
    public void Init(SessionsPresenter presenter) {
        this.presenter = presenter;
        startNewSessionButton.onClick.RemoveAllListeners();
        startNewSessionButton.onClick.AddListener(StartNewSession);
        OpponentPopup.SetActive(false);

        RefreshActiveSessionsButton.onClick.RemoveAllListeners();
        RefreshActiveSessionsButton.onClick.AddListener(RefreshSessions);
        RefreshFinishedSessionsButton.onClick.RemoveAllListeners();
        RefreshFinishedSessionsButton.onClick.AddListener(RefreshSessions);

        RefreshActiveSessionsButton.interactable = false;
        RefreshFinishedSessionsButton.interactable = false;
    }

    public void ShowActiveSessions(List<ActiveSessionDTO> activeSessions) {
        var oldSessions = activeSessionContainer.GetComponentsInChildren<ActiveSessionRowView>();
        foreach (ActiveSessionRowView oldSession in oldSessions) {
            Destroy(oldSession.gameObject);
        }

        activeSessionsTitle.SetActive(activeSessions.Count > 0);
        activeSessionsFooter.SetActive(activeSessions.Count > 0);

        activeSessions = activeSessions.OrderByDescending(x => x.isLocalPlayer).ToList();

        foreach (var session in activeSessions) {
            ActiveSessionRowView row = Instantiate(ActiveSessionsRowPrefab, activeSessionContainer);
            row.Initialize(session, presenter.PlaySession);
        }

        activeSessionsDone = true;
        CheckForFinish();

    }

    public void ShowOpponent(string name) {
        OpponentPopup.SetActive(true);
        OpponentPopupName.text = name;
    }

    public void ShowFinishedSessions(List<FinishedSessionDto> finishedSessions) {
        var oldFinishedSessions = finishedSessionContainer.GetComponentsInChildren<FinishedSessionsView>();
        foreach (FinishedSessionsView oldSession in oldFinishedSessions) {
            Destroy(oldSession.gameObject);
        }

        finishSessionsTitle.SetActive(finishedSessions.Count > 0);
        finishSessionsFooter.SetActive(finishedSessions.Count > 0);

        finishedSessions = finishedSessions.OrderByDescending(x => x.needShowResult).ToList();

        foreach (var session in finishedSessions) {
            FinishedSessionsView row = Instantiate(FinishedSessionRowPrefab, finishedSessionContainer);
            row.Initialize(session, presenter.PlayRematch, presenter.ShowResult);
        }

        finishedSessionsDone = true;
        CheckForFinish();
    }

    public void DoRefreshButtonsInteractable() {
        RefreshActiveSessionsButton.interactable = true;
        RefreshFinishedSessionsButton.interactable = true;
    }

    private void CheckForFinish() {
        if (activeSessionsDone && finishedSessionsDone) {
            transitionScreen.SetActive(false);
            Invoke(nameof(DoRefreshButtonsInteractable), 5);
        }
    }

    public void ShowPlayer(string name) {
        UserPanel.Init(name);
    }

    public void StartNewSession() {
        presenter.StartNewSession();
    }

    public void ShowSessionResult(SessionResultDTO sessionResult) {
        sessionResultUI.Init(sessionResult);
    }

    public void RefreshSessions() {
        RefreshActiveSessionsButton.interactable = false;
        RefreshFinishedSessionsButton.interactable = false;
        presenter.RefreshSessions();
    }
}

