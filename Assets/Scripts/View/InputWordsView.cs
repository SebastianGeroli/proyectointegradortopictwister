using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class InputWordsView : MonoBehaviour, IInputWordsView 
{
    [SerializeField] private InputWordsFieldUI inputWordsFieldPrefab;
    private List<InputWordsFieldUI> inputWordFields;
    [SerializeField] private Transform inputWordFieldsParent;
    [SerializeField] private Button finishButton;
    [SerializeField] private TMP_Text finishButtonText;
    [SerializeField] private TMP_Text letterText;
    [SerializeField] private UserPanelView userPanal;
    [SerializeField] private GameObject popUpTimeOver;
    [SerializeField] private GameObject transitionScreen;

    private InputWordsPresenter presenter;

    public void Init(InputWordsPresenter presenter)
    {
        this.presenter = presenter;
        gameObject.SetActive(true);
        inputWordFields = new List<InputWordsFieldUI>(inputWordFieldsParent.GetComponentsInChildren<InputWordsFieldUI>());
        for (int i = 0; i < presenter.Categories.Count; i++)
        {
            while (inputWordFields.Count <= i) {
                inputWordFields.Add(Instantiate(inputWordsFieldPrefab, inputWordFieldsParent));
            }
            inputWordFields[i].Init(presenter.Categories[i], presenter.Words[i], i, OnWordChanged);
        }
        finishButton.onClick.AddListener(FinishInputs);

        for (int i = presenter.Categories.Count; i < inputWordFields.Count; i++) {
            inputWordFields[i].gameObject.SetActive(false);
        }
    }

    public void ShowPlayerName(string playerName) {
        userPanal.Init(playerName);
    }

    public string OnWordChanged(string word, int index) {
        return presenter.OnWordChanged(word, index);
    }

    public void FinishInputs() {
        presenter.FinishTurn();
    }

    public void DisableInputs() {
        inputWordFields.ForEach(input => input.DisableInput());
        transitionScreen.SetActive(true);
    }

    public void ChangeButtonText(string text) {
        finishButton.gameObject.SetActive(false);
        finishButtonText.text = text;
    }

    public void ShowTimeOverMessage(float time) {
        popUpTimeOver.SetActive(true);
        Invoke(nameof(CloseTimeOverPopUp), time);
    }

    private void CloseTimeOverPopUp() {
        popUpTimeOver.SetActive(false);
    }

    public void ShowLetter(string letter) {
        letterText.text = ("LETRA: " + letter).ToUpper();
    }
}
