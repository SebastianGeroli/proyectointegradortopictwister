using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class RoundResultView : MonoBehaviour, IRoundResultView 
{
    [SerializeField] private TMP_Text titleTxt;
    [SerializeField] TMP_Text player1NameTxt;
    [SerializeField] TMP_Text player1TimeTxt;
    [SerializeField] TMP_Text player2NameTxt;
    [SerializeField] TMP_Text player2TimeTxt;

    [SerializeField] Toggle winner1;
    [SerializeField] Toggle winner2;

    [SerializeField] private CategoryRoundResultUI roundResultsUIPrefab;
    [SerializeField] private Transform roundResultContainer;
    [SerializeField] private GameObject transitionScreen;

    private  List<CategoryRoundResultUI> roundResultsUIs;

    [SerializeField] Button bottonButton;

    private RoundResultPresenter presenter;

    public void Init(RoundResultPresenter presenter) {
        this.presenter = presenter ;

        bottonButton.onClick.RemoveAllListeners();
        bottonButton.onClick.AddListener(this.presenter.CloseResult);
    }

    public void ShowTitle(string title) => titleTxt.text = title;

    public void ShowResult(RoundResultDTO result) {
        player1NameTxt.text = result.player1Name;
        player2NameTxt.text = result.player2Name;
        winner1.isOn = result.isPlayer1Winner;
        winner2.isOn = result.isPlayer2Winner;

        player1TimeTxt.gameObject.SetActive(result.player1Time>0);
        player2TimeTxt.gameObject.SetActive(result.player2Time>0);

        player1TimeTxt.text = result.player1Time.ToString("0.00s");
        player2TimeTxt.text = result.player2Time.ToString("0.00s");

        ShowResults(result.roundResults);
    }

    public void ShowResults(List<RoundResultByCategoryDTO> results) {
        roundResultsUIs = new List<CategoryRoundResultUI>(roundResultContainer.GetComponentsInChildren<CategoryRoundResultUI>());
        for (int i = 0; i < results.Count; i++) {
            if (roundResultsUIs.Count < i) {
                roundResultsUIs.Add(Instantiate(roundResultsUIPrefab, roundResultContainer));
            } else {
                roundResultsUIs[i].gameObject.SetActive(true);
            }
            roundResultsUIs[i].Init(results[i]);
        }

        for (int i = results.Count; i < roundResultsUIs.Count; i++) {
            roundResultsUIs[i].gameObject.SetActive(false);
        }
        transitionScreen.SetActive(false);
    }
}
