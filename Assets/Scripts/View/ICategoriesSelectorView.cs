﻿using System.Collections.Generic;

public interface ICategoriesSelectorView
{
    public void Init(CategoriesSelectorPresenter presenter);
    public void ShowCategories(List<string> categories);
    public void ShowPlayer(string playerName);
}