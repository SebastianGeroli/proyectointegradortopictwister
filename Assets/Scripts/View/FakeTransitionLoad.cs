using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FakeTransitionLoad : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text;
    private int position = 0;
    [SerializeField] private string textValue ="Cargando";

    private void OnEnable()
    {
        position = 0;
        text.text = textValue;
        StartCoroutine(LoadingAnimation());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }


    private IEnumerator LoadingAnimation()
    {
        while (true)
        {
            switch (position)
            {
                case 3:
                    text.text = $"... {textValue} ...";
                    break;
                case 2:
                    text.text = $".. {textValue} ..";
                    break;
                case 1:
                    text.text = $". {textValue} .";                   
                    break;
                default:
                    text.text = $"{textValue}";
                    break;
            }

            yield return new WaitForSeconds(0.4f);

            position++;
            if (position > 2)
            {
                position = 0;
            }
        }
    }
}