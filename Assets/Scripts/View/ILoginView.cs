﻿using System;

public interface ILoginView
{
    public void Init(LoginPresenter loginPresenter);
    public void ShowErrorMessage(string message);
    public void ShowPlayerName(string name);
}