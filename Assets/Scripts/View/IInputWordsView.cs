﻿using System.Collections.Generic;
using UnityEngine.Events;

public interface IInputWordsView
{
    void Init(InputWordsPresenter presenter);
    void ShowPlayerName(string playerName);
    void ShowLetter(string letter);
    void DisableInputs();
    void ChangeButtonText(string text);
    void ShowTimeOverMessage(float time);
}