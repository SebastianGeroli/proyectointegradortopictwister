﻿public interface ILetterSelectorView
{
    public void Init(LetterSelectorPresenter presenter);
    public void ShowLetter(string letter);
}