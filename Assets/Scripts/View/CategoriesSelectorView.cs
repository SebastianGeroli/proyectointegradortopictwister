using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CategoriesSelectorView : MonoBehaviour, ICategoriesSelectorView
{
    private CategoriesSelectorPresenter presenter;
    private List<CategorySelectorUI> categoriesUIs;

    [SerializeField] private UserPanelView userPanel;
    [SerializeField] private CategorySelectorUI categorySelectorPrefab;

    [SerializeField] private Transform categoriesContainer;

    public void Init(CategoriesSelectorPresenter presenter) {
        this.presenter = presenter;
        categoriesUIs = new List<CategorySelectorUI>(categoriesContainer.GetComponentsInChildren<CategorySelectorUI>());
    }

    public void FinishSelection() {
        presenter.ConfirmCategories();
    }

    public void ShowCategories(List<string> categories) {
        for (int i = 0; i < categories.Count; i++) {
            if (categoriesUIs.Count < i) {
                categoriesUIs.Add(Instantiate(categorySelectorPrefab, categoriesContainer));
            } else {
                categoriesUIs[i].gameObject.SetActive(true);
            }
            categoriesUIs[i].Init(categories[i]);
        }

        for (int i = categories.Count; i < categoriesUIs.Count; i++) {
            categoriesUIs[i].gameObject.SetActive(false);
        }
    }

    public void ShowPlayer(string playerName) {
        userPanel.Init(playerName);
    }
}
