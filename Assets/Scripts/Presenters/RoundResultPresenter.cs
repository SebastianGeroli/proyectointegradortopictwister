using System;
using UnityEngine;

public class RoundResultPresenter
{
    protected RoundResultDTO roundResultDTO;
    protected IRoundResultView view;
    protected ISessionManager sessionManager;

    public event Action OnPresenterFinished;

    public RoundResultPresenter(IRoundResultView view, ISessionManager sessionManager)
    {
        this.sessionManager = sessionManager;
        this.view = view;
    }

    public void GetRoundResult(Round round)
    {
        sessionManager.GetRoundResult(round, OnRoundResultReceived, OnError); 
    }

    public virtual void Init()
    {
        view.Init(this);
        GetRoundResult(sessionManager.CurrentSession.CurrentRound);
        view.ShowTitle("Resultados");
    }

    public void OnRoundResultReceived(RoundResultDTO roundResult) {
        roundResultDTO = roundResult;
        view.ShowResult(roundResultDTO);
    }

    public void OnError(string message) {
        Debug.LogError(""+message);
    }

    public void CloseResult() {
        OnPresenterFinished?.Invoke();
    }
}
