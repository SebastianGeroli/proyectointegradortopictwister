using System;
using UnityEngine;

public class PreviousRoundResultPresenter : RoundResultPresenter
{
    public PreviousRoundResultPresenter(IRoundResultView view, ISessionManager sessionManager) : base(view, sessionManager) {
    }


    public override void Init()
    {
        view.Init(this);

        Round previousRound = sessionManager.CurrentSession.PreviousRound;
        GetRoundResult(previousRound);

        view.ShowTitle("Resultado\n<size= 36>Ronda Anterior</size>");
    }
}
