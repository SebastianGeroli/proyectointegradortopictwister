﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class LetterSelectorPresenter
{
    private ISessionManager sessionManager;
    private ILetterSelectorView view;
    private ILetterRepository letterRepository;
    LetterManager letterManager;

    private Letter currentLetter;
    public string CurrentLetter { get => currentLetter!= null ? currentLetter.Char : "";}
    private bool randomLetterClicked = false;

    public event Action OnSelectedLetter;

    public LetterSelectorPresenter(ILetterSelectorView view, ISessionManager sessionManager, ILetterRepository letterRepository) {
        this.view = view;
        this.sessionManager = sessionManager;
        this.letterRepository = letterRepository;
        letterManager = new LetterManager(letterRepository);

        view.Init(this);
    }

    public void GetRandomLetter()
    {
        if(randomLetterClicked) return;
        randomLetterClicked = true;
        letterManager.GetRandomLetter(OnLetterReceived);
    }

    public void OnLetterReceived(Letter letter) {
        currentLetter = letter;
        view.ShowLetter(currentLetter.Char);

        sessionManager.SetLetterCurrentRound(letter, OnSavedLetter, OnError);
    }

    public void OnSavedLetter() {
        OnSelectedLetter?.Invoke();
    }

    public void OnError(string message) {
        Debug.LogError(message);
    }

    public void SelectRandomLetter() {
        GetRandomLetter();
    }

    
}