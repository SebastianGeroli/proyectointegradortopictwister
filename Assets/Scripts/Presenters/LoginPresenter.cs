﻿using System;
using System.Threading.Tasks;

public class LoginPresenter
{
    private ILoginView loginView;
    private ISessionManager sessionManager;
    private PlayerManager playerManager;

    public event Action OnComplete;

    public LoginPresenter(ILoginView loginView, ISessionManager sessionManager,IPlayerRepository playerRepository) {
        this.loginView = loginView;
        this.sessionManager = sessionManager;
        playerManager = new PlayerManager(playerRepository);
        loginView.Init(this);
        if (PlayerManager.CurrentPlayer != null) {
            loginView.ShowPlayerName(PlayerManager.CurrentPlayer.Name);
        } else {
            loginView.ShowPlayerName("");
        }
    }

    public void Login(string name, string password) {
        playerManager.LoginPlayer(name,password, OnLogin, OnLoginError);
    }

    public void CreatePlayer(string name, string password, string confirmPassword) {
        if (password == confirmPassword) {
            playerManager.CreatePlayer(name, password, OnLogin, OnLoginError);
        } else {
            OnLoginError("Las contraseñas deben ser iguales");
        }
    }

    public void OnLogin(Player player) {
        loginView.ShowPlayerName(player.Name);

        //sessionManager.ResetSessions();
        sessionManager.Login(player);
        OnComplete?.Invoke();
    }

    public void OnLoginError(string message) {
        loginView.ShowErrorMessage(message.ToString());
    }
}