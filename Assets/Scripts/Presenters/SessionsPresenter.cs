using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using DTOs;
using UnityEngine;
using UnityEngine.Analytics;

public class SessionsPresenter
{
    private ISessionsView view;
    private ISessionManager sessionManager;
    private List<ActiveSessionDTO> activeSessions;
    private List<FinishedSessionDto> finishedSessions;
    public List<ActiveSessionDTO> ActiveSessions { get => activeSessions; }

    public event Action<Session> OnActiveSessionSelected;

    private float minTimeToShowTheOpponent;
    private float initialTimeBeforeShowTheOpponent;

    public SessionsPresenter(ISessionsView view, ISessionManager sessionManager, float minTimeToShowTheOpponent) {
        this.view = view;
        this.sessionManager = sessionManager;
        this.minTimeToShowTheOpponent = minTimeToShowTheOpponent;
        InitView();
    }

    private void InitView() {
        view.Init(this);
        view.ShowPlayer(sessionManager.LocalPlayer.Name);

        RefreshSessions();
    }

    public void RefreshSessions() {
        GenerateActiveSessions();
        GenerateInactiveSessions();
    }

    private void GenerateInactiveSessions() {
        sessionManager.GetFinishedSessions(OnFinishedSessions, OnErrorGetFinishedSessions);
    }

    private void OnFinishedSessions(List<FinishedSessionDto> finishedSessions) {
        this.finishedSessions = finishedSessions;
        view.ShowFinishedSessions(this.finishedSessions);

        FinishedSessionDto finishedSessionToShowResult = sessionManager.ShouldShowSomeResult(finishedSessions);
        if(finishedSessionToShowResult != null) {
            ShowResult(finishedSessionToShowResult);
        }
    }


    public void GenerateActiveSessions() {
        sessionManager.GetActiveSessions(OnActiveSessions, OnErrorGetSessions);
    }

    public void OnActiveSessions(List<ActiveSessionDTO> activeSessions) {
        this.activeSessions = activeSessions;
        view.ShowActiveSessions(activeSessions);
    }

    public void PlaySession(ActiveSessionDTO activeSession) {
        ShowOpponentName(activeSession.opponentName);
        sessionManager.PlaySession(activeSession.sessionID, OnSessionReceived, OnErrorGetSession);
    }

    public async void OnSessionReceived(Session session) {
        //wait at least minTimeToShowTheOpponent to start the session
        float elapsedTime = Time.realtimeSinceStartup - initialTimeBeforeShowTheOpponent;
        if (elapsedTime < minTimeToShowTheOpponent) {
            await Task.Delay((int)(minTimeToShowTheOpponent - elapsedTime) * 1000);
        }

        OnActiveSessionSelected?.Invoke(session);
    }

    public async void StartNewSession() {
        await sessionManager.StartNewSession(ShowOpponentName);

        OnSessionReceived(sessionManager.CurrentSession);
    }

    public void ShowOpponentName(string opponentName) {
        initialTimeBeforeShowTheOpponent = Time.realtimeSinceStartup;
        view.ShowOpponent(opponentName);
    }

    public void PlayRematch(FinishedSessionDto finishedSession) {
        ShowOpponentName(finishedSession.opponentName);
        sessionManager.PlayRematch(finishedSession.opponentID, OnSessionReceived, OnErrorGetSession);
    }
    public void ShowResult(FinishedSessionDto dto) {
        sessionManager.GetSessionResult(dto.sessionID, OnSessionResultReceived, OnErrorGetResult);
    }
    private void OnSessionResultReceived(SessionResultDTO dto) {
        view.ShowSessionResult(dto);
    }

    private void OnErrorGetFinishedSessions(string error) => Debug.LogError(error);
    public void OnErrorGetSessions(string error) => Debug.LogError(error);
    public void OnErrorGetSession(string error) => Debug.LogError(error);
    public void OnErrorGetResult(string error) => Debug.LogError(error);

}
