using System;
using System.Collections.Generic;

public class CategoriesSelectorPresenter
{
    private Player player;
    public List<Category> categories { get; private set; }
    public List<string> CategoriesNames { get; private set; }
    public string PlayerName => player.Name;

    private Round currentRound;
    private ICategoriesSelectorView categoriesSelectorView;

    private CategoryManager categoryManager;

    public event Action OnCategoriesConfirmed;

    public CategoriesSelectorPresenter(ICategoriesSelectorView categoriesSelectorView, Round currentRound, ICategoryRepository categoryRepository) {
        this.currentRound = currentRound;
        player = currentRound.CurrentPlayer;
        this.categoriesSelectorView = categoriesSelectorView;

        categoryManager = new CategoryManager(categoryRepository);
        InitView();

    }

    private void InitView() {
        categoriesSelectorView.Init(this);
        categoriesSelectorView.ShowCategories(new List<string>());
        categoriesSelectorView.ShowPlayer(PlayerName);

        GetRandomCategories();
    }


    public void GetRandomCategories() {
        categoryManager.GetRandomCategories(5, OnRandomCategoriesReceived);
    }

    public void OnRandomCategoriesReceived(List<Category> categories) {
        this.categories = categories;
        CategoriesNames = CategoryManager.GetCategoriesNames(categories);
        categoriesSelectorView.ShowCategories(CategoriesNames);

        //Auto Select
        ConfirmCategories();
    }

    public void ConfirmCategories() {
        currentRound.Categories = categories;
        OnCategoriesConfirmed?.Invoke();
    }

}
