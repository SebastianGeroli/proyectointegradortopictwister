using System;
using System.Collections.Generic;
using UnityEngine;

public class InputWordsPresenter
{
    public event Action OnFinishedTurn;

    private IInputWordsView view;
    private ISessionManager sessionManager;

    private List<Word> words;
    public List<string> Words {
        get { 
            var list = new List<string>();
            foreach(var word in words) {
                list.Add(word.Name);
            }
            return list;
        }
    }

    private List<string> categories;
    public List<string> Categories { get => categories; }

    private ITimer timerController;
    private Turn currentTurn;
    private Round currentRound;

    public InputWordsPresenter(IInputWordsView view, ISessionManager sessionManager, ITimer timerController) {
        this.view = view;
        this.timerController = timerController;
        this.sessionManager = sessionManager;
        this.currentRound = sessionManager.CurrentSession.CurrentRound;
        currentTurn = currentRound.CurrentTurn;
        categories = CategoryManager.GetCategoriesNames(currentRound.Categories);

        words = GetListWithEmptyWords(currentRound.Categories.Count);

        view.ShowPlayerName(currentTurn.Player.Name);
        view.ShowLetter(this.currentRound.Letter.Char) ;
    }

    public void Init() {
        view.Init(this);

        timerController.StartCountDown(60f, OnTimeOver);
    }

    public string OnWordChanged(string text, int index) {
        return ReplaceWord(text, index);
    }

    private string ReplaceWord(string text, int index) {
        words[index] = new Word(text);
        return words[index].Name;
    }

    private List<Word> GetListWithEmptyWords(int categoriesCount) {
        List<Word> list = new List<Word>(categoriesCount);
        for (int i = 0; i < categoriesCount; i++) {
            list.Add(new Word(""));
        }
        return list;
    }

    public void FinishTurn() {
        view.DisableInputs();
        view.ChangeButtonText("Ver resultados");

        timerController.StopCountDown();

        sessionManager.FinishTurn(timerController.CurrentTime, words, currentRound, OnWordsSended, OnWordSendFailed);
    }

    public void OnWordsSended() {

        //if (currentRound.CurrentPlayer is BotPlayer bot)
        //    BotManager.CompleteTurn(currentRound, currentRound.CurrentTurn, bot);

        OnFinishedTurn?.Invoke();
    }

    public void OnWordSendFailed(string message) {
        Debug.LogError(message);
    }

    public void OnTimeOver() {
        view.ShowTimeOverMessage(3);
        FinishTurn();
    }

}
