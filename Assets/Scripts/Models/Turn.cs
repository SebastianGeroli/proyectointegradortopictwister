﻿using System;
using System.Collections.Generic;

public abstract class Turn
{
    public string ID { get; set; }

    private Player player;
    public virtual Player Player {
        get { return player; }
        protected set { player = value; }
    }

    private List<Answer> answers = new List<Answer>();
    public List<Answer> Answers {
        get { return answers; }
        protected set { answers = value; }
    }

    public abstract bool Finished { get; }

    public abstract float FinishTime { get; protected set; }

    public abstract int CorrectAnswers { get; }


    public abstract void FinishTurn(float time, List<Word> words, Round round);
}