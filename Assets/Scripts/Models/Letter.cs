﻿public class Letter
{ 
    public string ID { get; protected set; }
    public string Char { get; protected set; } = " ";

    public Letter(string letter) {
        Char = letter;
        ID = letter.ToString();
    }

    public Letter(char letter) {
        Char = letter.ToString();
        ID = letter.ToString();
    }
    public Letter(string letter, string id) {
        Char = letter;
        this.ID = id;
    }
}