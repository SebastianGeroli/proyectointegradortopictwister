using DTOs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoteTurn : Turn
{

    private bool finished;
    public override bool Finished { get => finished; }

    public override float FinishTime { get; protected set; }

    public override int CorrectAnswers { get; }

    public RemoteTurn(TurnDto turn) {
        ID = turn.turnID;
        Player = turn.playerID!=null && turn.playerID == RemoteSessionManager.Instance.LocalPlayer.ID ? RemoteSessionManager.Instance.LocalPlayer: null;
        finished = turn.finished;
        FinishTime = turn.finishTime;
        CorrectAnswers = turn.correntAnswers;

        turn.answers?.ForEach(answer => {
            Answers.Add(new Answer(answer.wordAnswered, new Category("TODO REMOTE", answer.categoryID), answer.correct));
        });
    }

    public override void FinishTurn(float time, List<Word> words, Round round) {
        FinishTime = time >= 0 ? time : 0f;
        finished = true;
    }

}
