﻿public class LocalRound : Round
{

    public override bool Finished {
        get {
            return Turn1.Finished && Turn2.Finished;
        }
    }

    public override Player[] Winner => GetWinners();

    public override Player CurrentPlayer => GetCurrentPlayer();

    public override Turn CurrentTurn => GetCurrentTurn();

    private Turn GetCurrentTurn() {
        if (!Turn1.Finished) {
            return Turn1;
        } else if (!Turn2.Finished) {
            return Turn2;
        }

        return null;
    }

    private Player GetCurrentPlayer() {

        if (!Turn1.Finished) {
            return Turn1.Player;
        } else if (!Turn2.Finished) {
            return Turn2.Player;
        }

        return null;
    }

    private Player[] GetWinners() {
        if (!Finished) return new Player[0];

        if (Turn1.CorrectAnswers > Turn2.CorrectAnswers) {
            return new Player[] { Turn1.Player };
        } else if (Turn1.CorrectAnswers < Turn2.CorrectAnswers) {
            return new Player[] { Turn2.Player };
        } else {
            if (Turn1.FinishTime > Turn2.FinishTime) {
                return new Player[] { Turn1.Player };
            } else if (Turn1.FinishTime < Turn2.FinishTime) {
                return new Player[] { Turn2.Player };
            } else {
                return new Player[] { Turn1.Player, Turn2.Player };
            }
        }
    }

    public LocalRound(Player player1, Player player2) {
        ID = GetHashCode().ToString();
        Turn1 = new LocalTurn(player1);
        Turn2 = new LocalTurn(player2);
    }

    public LocalRound(Turn turn1, Turn turn2) {
        ID = GetHashCode().ToString();
        Turn1 = turn1;
        Turn2 = turn2;
    }
}
