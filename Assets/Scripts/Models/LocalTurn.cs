﻿using System;
using System.Collections.Generic;

public class LocalTurn : Turn
{

    public override float FinishTime { get; protected set; } = -1f;

    public override bool Finished => FinishTime >= 0 ? true : false;

    public override int CorrectAnswers => GetAmountOfCorrectAnswers();

    public LocalTurn(Player player)
    {
        Player = player;
    }

    public override void FinishTurn(float time, List<Word> words, Round round)
    {
        FinishTime = time >= 0 ? time : 0f;
        GameDictionary gameDictionary = new GameDictionary();
        IWordValidator wordValidator = new WordValidator(gameDictionary);
        if (words == null) return;
        for (int i = 0; i < words.Count; i++)
        {
            string word = words[i].Name;
            Category category = round.Categories[i];
            bool isCorrect = wordValidator.ValidateWord(word, category, round.Letter);
            Answer answer = new Answer(word, category, isCorrect);
            Answers.Add(answer);
        }
    }

    private int GetAmountOfCorrectAnswers()
    {
        if (Answers == null || Answers.Count == 0) return 0;

        int result = 0;
        foreach (var answer in Answers)
        {
            if (answer.IsCorrect)
            {
                result++;
            }
        }
        return result;
    }
}
