﻿using DTOs;
using System.Collections.Generic;

internal class RemoteRound : Round
{
    private List<Turn> turns;
    public override bool Finished { get; }

    public override Player[] Winner { get; }

    public override Player CurrentPlayer { get; }

    public override Turn CurrentTurn { get; }

    public RemoteRound(RoundDto round) {
        ID = round.roundID;
        turns = new List<Turn>();
        round.turns?.ForEach(turn => {
            turns.Add(new RemoteTurn(turn));
        });
        if (turns.Count > 0) Turn1 = turns[0];
        if (turns.Count > 1) Turn2 = turns[1];

        Finished = round.finished;
        CurrentTurn = new RemoteTurn(round.currentTurn);
        if (round.currentPlayer != null && round.currentPlayer.playerID!= null) {
            CurrentPlayer = new RemotePlayer(round.currentPlayer);
        } else {
            CurrentPlayer = CurrentTurn.Player;
        }

        Categories = new List<Category>();
        round.categories?.ForEach(category => {
            Categories.Add(new Category(category.categoryName,category.categoryID));
        });

        Letter = new Letter(round.letter?.letterName, round.letter?.letterID);
    }
}