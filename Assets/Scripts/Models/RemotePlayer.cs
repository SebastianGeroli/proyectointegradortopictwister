﻿using DTOs;
using System;

public class RemotePlayer : Player
{
    public RemotePlayer(PlayerDto dto) {
        Name = dto.playerName;
        ID = dto.playerID;
    }

    public RemotePlayer(string playerID,string playerName, string password = "") {
        Name = playerName;
        ID = playerID;
        Password = password;
    }

   
    
}