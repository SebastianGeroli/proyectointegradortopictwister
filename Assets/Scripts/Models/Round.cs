﻿using System.Collections.Generic;

public abstract class Round
{
    public string ID { get; protected set; }

    private Turn turn1;
    public virtual Turn Turn1 {
        get { return turn1; } 
        protected set { turn1 = value; } 
    }

    private Turn turn2;
    public virtual Turn Turn2 {
        get { return turn2; }
        protected set { turn2 = value; }
    }

    public abstract bool Finished { get; }

    public abstract Player[] Winner { get; }

    public abstract Player CurrentPlayer { get; }

    public abstract Turn CurrentTurn { get; }

    private List<Category> categories = new List<Category>();
    public List<Category> Categories {
        get => categories;

        set {
            if (categories.Count == 0) {
                categories = value;
            }
        }
    }

    private Letter letter;
    public Letter Letter {
        get {
            return letter;
        }
        set {
            if (letter == null || letter.Char==null || letter.ID ==null || letter.Char.Trim() == "") {
                letter = value;
            }
        }
    }
}
