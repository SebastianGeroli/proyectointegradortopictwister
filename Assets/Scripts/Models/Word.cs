﻿using System.Collections.Generic;
using System.Globalization;
using System.Text;

public class Word
{
    List<Category> categories = new List<Category>();
    public List<Category> Categories => categories;

    private string name;
    public string Name { get => name; }

    public Word(string wordName, List<Category> categories) : this(wordName) {
        this.categories = categories;
    }
    public Word(string wordName, params string[] categories) : this(wordName) {
        foreach (var category in categories) {
            this.categories.Add(new Category(category));
        }
    }

    public Word(string text) {
        name = FilterNoLetterOrNumber(text);
        //name = RemoveDiacritics(name);
        name = name.ToUpper();
        name = name.Trim();
    }

    private string FilterNoLetterOrNumber(string word) {
        StringBuilder filteredWord = new StringBuilder();
        for (int i = 0; i < word.Length; i++) {
            if (char.IsLetter(word[i]) || char.IsWhiteSpace(word[i]) || char.IsNumber(word[i])) {
                filteredWord.Append(word[i]);
            }
        }
        return filteredWord.ToString();
    }


    private string RemoveDiacritics(string text) {
        StringBuilder stringBuilder = new StringBuilder();
        string normalizedtext = text.Normalize(NormalizationForm.FormD);
        for (int i = 0; i < normalizedtext.Length; i++) {
            UnicodeCategory unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(normalizedtext[i]);
            if (unicodeCategory != UnicodeCategory.NonSpacingMark) {
                stringBuilder.Append(normalizedtext[i]);
            }
        }
        return stringBuilder.ToString();
    }
}