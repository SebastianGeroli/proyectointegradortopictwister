using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDictionary
{
    private List<Word> words = new List<Word>();
    public List<Word> Words => words;

    public GameDictionary(List<Word> words)
    {
        this.words = words;
    }

    public GameDictionary() {
        words.Add(new Word("aguila", "Animal"));
        words.Add(new Word("elefante", "Animal"));
        words.Add(new Word("iguana", "Animal"));
        words.Add(new Word("oso", "Animal"));
        words.Add(new Word("urraca", "Animal"));
        words.Add(new Word("anillo", "Objeto"));
        words.Add(new Word("espejo", "Objeto"));
        words.Add(new Word("iman", "Objeto"));
        words.Add(new Word("olla", "Objeto"));
        words.Add(new Word("urna", "Objeto"));
        words.Add(new Word("avengers", "Cine"));
        words.Add(new Word("eragon", "Cine"));
        words.Add(new Word("inframundo", "Cine"));
        words.Add(new Word("oblivion", "Cine"));
        words.Add(new Word("ulises", "Cine"));
        words.Add(new Word("adele", "Musica"));
        words.Add(new Word("elvis", "Musica"));
        words.Add(new Word("inna", "Musica"));
        words.Add(new Word("oasis", "Musica"));
        words.Add(new Word("usher", "Musica"));
        words.Add(new Word("anana", "Fruta o Verdura"));
        words.Add(new Word("espinaca", "Fruta o Verdura"));
        words.Add(new Word("iceberg", "Fruta o Verdura"));
        words.Add(new Word("olivo", "Fruta o Verdura"));
        words.Add(new Word("uva", "Fruta o Verdura"));
    }

    public Word GetWordForCategory(Category category, Letter letter) {
        string letterAsString = letter.Char.ToString().ToUpper();
        List<Word> foundWords = words.FindAll(word => word.Name.StartsWith(letterAsString) && word.Categories.Contains(category));
        if (foundWords.Count > 0) {
            return foundWords[Random.Range(0, foundWords.Count)];
        }
        return new Word("");
    }

    public Word FindWord(string word) {
        word = word.ToUpper();
        Word foundWord = words.Find(w => w.Name == word);
        if (foundWord != null)
            return foundWord;
        else
            return new Word(word);
    }

    public Word GetRandomWord() {        
        return Words[Random.Range(0, Words.Count)];
    }
}
