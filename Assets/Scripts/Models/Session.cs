﻿public abstract class Session
{
    public abstract string ID { get; }
    public abstract bool Finished { get; }

    public abstract Round CurrentRound { get; }

    private Round[] rounds;
    public virtual Round[] Rounds {
        get => rounds;
        protected set => rounds = value;
    }

    public Round PreviousRound {
        get {
            Round previousRound = null;
            foreach (var round in Rounds) {
                if (round.Finished) {
                    previousRound = round;
                } else {
                    break;
                }
            }

            return previousRound;
        }
    }

    public virtual Player GetOpponent(Player player) {
        return rounds[0].Turn1.Player == player ? rounds[0].Turn2.Player : rounds[0].Turn1.Player;
    }
}