﻿using System;

public abstract class Player : IEquatable<Player>
{
    private string id;
    public virtual string ID { get => id; protected set => id = value; }

    private string name;
    public virtual string Name { get => name; protected set => name = value; }

    private string password;
    public virtual string Password { get => password; protected set => password = value; }

   public bool Equals(Player other) {
        if (other == null)
            return false;

        if (this.ID == other.ID)
            return true;
        else
            return false;
    }

    public override bool Equals(Object obj) {
        if (obj == null)
            return false;

        Player personObj = obj as Player;
        if (personObj == null)
            return false;
        else
            return Equals(personObj);
    }

    public override int GetHashCode() {
        return this.ID.GetHashCode();
    }

    public static bool operator ==(Player player1, Player player2) {
        if (((object)player1) == null || ((object)player2) == null)
            return Object.Equals(player1, player2);

        return player1.Equals(player2);
    }

    public static bool operator !=(Player player1, Player player2) {
        if (((object)player1) == null || ((object)player2) == null)
            return !Object.Equals(player1, player2);

        return !(player1.Equals(player2));
    }
}