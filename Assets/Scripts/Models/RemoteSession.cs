﻿using DTOs;
using System.Collections.Generic;

internal class RemoteSession : Session
{
    private string id;
    public override string ID { get { return id; } }
    private bool finished;
    public override bool Finished { get { return finished; } }

    public override Round CurrentRound { 
        
        get {//TODO Get from API
            foreach (var round in Rounds) {
                if (round.Finished == false) {
                    return round;
                }
            }

            return null;
        } 
    }

    public RemoteSession(SessionDto dto) {
        id = dto.sessionID;
        List<Round> roundsFromDto = new List<Round>();
        dto.rounds.ForEach(round => {
            roundsFromDto.Add(new RemoteRound(round));
        });
        Rounds = roundsFromDto.ToArray();
        finished = dto.finished;
    }
}