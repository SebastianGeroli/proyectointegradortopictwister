using System;

public class Category : IEquatable<Category>
{
    private string id;
    public string ID { get => id; set => id = value; }

    private string name;
    public string Name { get => name; }

    public Category(string name) {
        this.name = name;
        id = name;
    }

    public Category(string name, string id) {
        this.name = name;
        this.id = id;
    }

    public bool Equals(Category other) {
        if (other == null)
            return false;

        if (this.ID == other.ID || this.Name.ToUpper() == other.name.ToUpper())
            return true;
        else
            return false;
    }
}

