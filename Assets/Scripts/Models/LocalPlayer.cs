﻿using System;
using System.ComponentModel;

public enum PlayerNameError
{
    [Description("Without errors")]
    NO_ERROR,
    [Description("The name is incorrect, must have at least 4 letters!")]
    WRONG_NAME
}

public class LocalPlayer : Player
{

    public LocalPlayer(string namePlayer)
    {
        Name = namePlayer;
        ID = Name;
    }

    public LocalPlayer(string namePlayer, string id)
    {
        ID = id;
        Name = namePlayer;
    }
    public static PlayerNameError VerifyName(string name)
    {
        if (name.Length >= 4)
            return PlayerNameError.NO_ERROR;
        return PlayerNameError.WRONG_NAME;
    }

}