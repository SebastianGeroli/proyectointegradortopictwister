﻿public class Answer
{
    private bool isCorrect;
    public bool IsCorrect { get => isCorrect; }

    private string word;
    public string Word { get => word; }

    private Category category;
    public Category Category { get => category; }

    public Answer(string word, Category category, bool isCorrect) {
        this.word = word;
        this.category = category;
        this.isCorrect = isCorrect;
    }
}
