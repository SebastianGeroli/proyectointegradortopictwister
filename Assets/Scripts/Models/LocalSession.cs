﻿public class LocalSession : Session {

    public LocalSession(Player player1, Player player2, int amountOfRounds) {
        Rounds = new LocalRound[amountOfRounds];
        for (int i = 0; i < amountOfRounds; i++) {
            if (i % 2 == 0) {
                Rounds[i] = new LocalRound(player1, player2);
            } else {
                Rounds[i] = new LocalRound(player2, player1);
            }
        }
    }

    public LocalSession(Round[] rounds) => Rounds = rounds;

    public override bool Finished => GetIsFinished();

    public override Round CurrentRound => GetCurrentRound();

    public override string ID => GetHashCode().ToString();

    private Round GetCurrentRound() {
        foreach (var round in Rounds) {
            if (round.Finished == false) {
                return round;
            }
        }

        return null;
    }

    private bool GetIsFinished() {
        foreach (var round in Rounds) {
            if (round.Finished == false) {
                return false;
            }
        }
        return true;
    }
}