﻿

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class BotPlayer : Player
{
    public const float MinSuccessProbability = 0.60f;
    public const float MaxSuccessProbability = 1f;
    GameDictionary _dictionary;
    public BotPlayer(string botName,GameDictionary dictionary) 
    {
        Name = botName;
        ID = Name;
        _dictionary = dictionary;
    }

    float successProbability;

    public float SuccessProbability { 
        get { return successProbability;  } 
        set {
            successProbability = Mathf.Clamp(value, MinSuccessProbability, MaxSuccessProbability);
        } 
    }

    public string GetRandomWord() {
        
        string word = _dictionary.GetRandomWord().Name;
        return word;
    }

    public string Answer(Category category, Letter letter)
    {        
        return _dictionary.GetWordForCategory(category, letter).Name;
    }

    public bool PredictSuccess(float successProbability)
    {
        
        return successProbability<=this.SuccessProbability;
    }
}