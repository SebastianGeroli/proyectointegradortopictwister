﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.APIs.AsyncAPIs;

public class ApiCategoryRepository : ICategoryRepository
{
    private List<Category> categories = new List<Category>();
    public async Task<List<Category>> GetCategories()
    {
        CategoryAsyncAPI api = new CategoryAsyncAPI();

        var dtos = await api.GetAll();
        var values = new List<Category>(dtos.Count);
        foreach (var dto in dtos)
        {
            Category category = new Category(dto.categoryName,dto.categoryID);
            values.Add(category);
        }

        return values;
    }

    public async Task<List<Category>> GetRandomCategories(int amountOfCategoriesAskedToReturn) {
        CategoryAsyncAPI api = new CategoryAsyncAPI();

        var dtos = await api.GetRandomCategories();

        var values = new List<Category>(dtos.Count);
        foreach (var dto in dtos) {
            Category category = new Category(dto.categoryName, dto.categoryID);
            values.Add(category);
        }

        return values;
    }
}
