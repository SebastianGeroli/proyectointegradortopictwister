﻿using System.Collections.Generic;
using System.Threading.Tasks;

public interface ILetterRepository
{
    public Task<List<Letter>> GetLetters();
    public Task<Letter> GetRandomLetter();
}