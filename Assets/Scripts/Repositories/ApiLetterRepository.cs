﻿using Backend.APIs.AsyncAPIs;
using DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

public class ApiLetterRepository : ILetterRepository
{
    public async Task<List<Letter>> GetLetters() {
        LetterAsyncAPI api = new LetterAsyncAPI();
        List<LetterDto> letters = await api.GetAllLetters();
        List<Letter> letterList = new List<Letter>(letters.Count);
        foreach (var letter in letters) {
            letterList.Add(new Letter(letter.letterName, letter.letterID));
        }
        return letterList;

    }
    public async Task<Letter> GetRandomLetter() {
        LetterAsyncAPI api = new LetterAsyncAPI();
        LetterDto letter = await api.GetRandomLetter();
        return new Letter(letter.letterName,letter.letterID);
    }
}
