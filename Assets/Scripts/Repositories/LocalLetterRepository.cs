﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

public class LocalLetterRepository : ILetterRepository
{
    List<Letter> list = new List<Letter>() {
            new Letter("a"),
            new Letter("e"),
            new Letter("i"),
            new Letter("o"),
            new Letter("u")
        };

    public async Task<List<Letter>> GetLetters() {
        return await Task.FromResult<List<Letter>>(list);
    }

    public async Task<Letter> GetRandomLetter() {
        Random rdm = new Random();
        var listOfLetters = await GetLetters();
        int randomIndex = rdm.Next(0, listOfLetters.Count);
        return listOfLetters[randomIndex];
    }
}
