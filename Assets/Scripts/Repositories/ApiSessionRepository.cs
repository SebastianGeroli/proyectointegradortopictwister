﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.APIs.AsyncAPIs;
using Backend.APIs.Shared;
using DTOs;

internal class ApiSessionRepository : ISessionRepository
{
    public async Task FinishTurn(string turnID, float time, List<string> wordsAsString, List<string> categoriesIDs, Action<string> onErrorCallback) {
        TurnAsyncAPI api = new TurnAsyncAPI();
        try {
            var turnDTO = await api.FinishTurn(turnID, time, wordsAsString, categoriesIDs);
        } catch (Exception e) {
            onErrorCallback?.Invoke(e.Message);
        }
    }

    public async Task<List<ActiveSessionDTO>> GetActiveSessions(string playerID,Action<string> onErrorCallback) {
        SessionAsyncAPI api = new SessionAsyncAPI();
        try {
            var dto = await api.GetActiveSessions(playerID);
            return dto;
        } catch (Exception e) {
            onErrorCallback?.Invoke(e.Message);
            return new List<ActiveSessionDTO>();
        }
    }

    public async Task<List<FinishedSessionDto>> GetFinishedSessions(string playerID,Action<string> onErrorCallback)
    {
        var api = new SessionAsyncAPI();
        try
        {
            var finishedSessions = await api.GetFinishedSessions(playerID);
            return finishedSessions;
        }
        catch (Exception e)
        {
            onErrorCallback(e.Message);
            return new List<FinishedSessionDto>();
        }
    }

    public async Task<RoundResultDTO> GetRoundResult(string roundID, Action<string> onErrorCallback) {
        RoundAsyncAPI api = new RoundAsyncAPI();
        try {
            RoundResultDTO dto = await api.GetRoundResult(roundID);
            return dto;
        } catch (Exception e) {
            onErrorCallback?.Invoke(e.Message);
            return null;
        }
    }

    public async Task<Session> CreateRematch(string localPlayerID, string opponentID, Action<string> onErrorGetSession)
    {
        var api = new SessionAsyncAPI();
        try
        {
            var sessionDto = await api.CreateRematch(localPlayerID, opponentID);
            var session = new RemoteSession(sessionDto);
            return session;
        }
        catch(Exception e)
        {
            onErrorGetSession(e.Message);
            return null;
        }
    }

    public async Task<Session> GetSession(string sessionID, Action<string> onErrorCallback) {
        SessionAsyncAPI api = new SessionAsyncAPI();
        try {
            var dto = await api.GetSession(sessionID);
            return new RemoteSession(dto) ;
        } catch (Exception e) {
            onErrorCallback?.Invoke(e.Message);
            return null;
        }
    }

    public async Task SetLetterAndCategories(string sessionID, string letterID, List<string> categoriesIDs, Action<string> onErrorCallback) {
        RoundAsyncAPI api = new RoundAsyncAPI();
        try {
            await api.SetLetterAndCategories(sessionID, letterID, categoriesIDs);
        } catch (Exception e) {
            onErrorCallback?.Invoke(e.Message);
        }
    }

    public async Task<SessionResultDTO> GetSessionResult(string sessionID, Action<string> onErrorCallback) {
        SessionAsyncAPI api = new SessionAsyncAPI();
        try {
            SessionResultDTO dto = await api.GetSessionResult(sessionID);
            return dto;
        } catch (Exception e) {
            onErrorCallback?.Invoke(e.Message);
            return null;
        }
    }
}