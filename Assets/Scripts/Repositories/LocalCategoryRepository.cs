﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

public class LocalCategoryRepository : ICategoryRepository
{

    private List<Category> categories = new List<Category>();

    public LocalCategoryRepository() {
        categories.Add(new Category("Objeto"));
        categories.Add(new Category("Cine"));
        categories.Add(new Category("Musica"));
        categories.Add(new Category("Animal"));
        categories.Add(new Category("Fruta o Verdura"));
    }

    public async Task<List<Category>> GetCategories() {
        return await Task.FromResult<List<Category>>(categories);
        //return categories;
    }

    public async Task<List<Category>> GetRandomCategories(int amountOfCategoriesAskedToReturn) {
        if (amountOfCategoriesAskedToReturn <= 0) {
            return await Task.FromResult<List<Category>>(new List<Category>());
        }

        Random random = new Random();
        List<Category> categoryList = new List<Category>();

        for (int i = 0; i < amountOfCategoriesAskedToReturn;) {
            int randomNumber = random.Next(0, categories.Count);
            if (i < categories.Count && !categoryList.Contains(categories[randomNumber])) {
                categoryList.Add(categories[randomNumber]);
                i++;
            } else if (i >= categories.Count) {
                categoryList.Add(categories[randomNumber]);
                i++;
            }
        }

        return await Task.FromResult<List<Category>>(categoryList);
    }
}