﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DTOs;

public interface ISessionRepository
{
    Task<List<ActiveSessionDTO>> GetActiveSessions(string playerID, Action<string> onErrorCallback);
    Task<List<FinishedSessionDto>> GetFinishedSessions(string playerID, Action<string> onErrorCallback);
    Task<Session> GetSession(string sessionID, Action<string> onErrorCallback);
    Task SetLetterAndCategories(string sessionID, string letterID, List<string> categoriesIDs, Action<string> onErrorCallback);
    Task FinishTurn(string iD, float time, List<string> wordsAsString, List<string> categoriesIDs, Action<string> onErrorCallback);
    Task<RoundResultDTO> GetRoundResult(string iD, Action<string> onErrorCallback);
    Task<Session> CreateRematch(string localPlayerID, string opponentID, Action<string> onErrorGetSession);
    Task<SessionResultDTO> GetSessionResult(string sessionID, Action<string> onErrorCallback);
}