using Backend.APIs.AsyncAPIs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class ApiPlayerRepository : IPlayerRepository
{
    public async Task<Player> CreatePlayer(string name, string password, Action<string> onErrorCallback) {
        PlayerAsyncAPI api = new PlayerAsyncAPI();
        try {
            var dto = await api.CreatePlayer(name, password);            
            return new RemotePlayer(dto);
        } catch (Exception e) {
            onErrorCallback?.Invoke(e.Message);
        }
        return null;
    }

    public async Task<Player> LoginPlayer(string name, string password, Action<string> onErrorCallback) {
        PlayerAsyncAPI api = new PlayerAsyncAPI();
        try {
            var dto = await api.LoginPlayer(name, password);
            return new RemotePlayer(dto);
        } catch (Exception e) {
            onErrorCallback?.Invoke(e.Message);
        }
        return null;
    }

    public async Task<Player> GetOpponent(Player currentPlayer, Action<string> onErrorCallback) {
        PlayerAsyncAPI api = new PlayerAsyncAPI();
        try {
            var dto = await api.GetOpponent(currentPlayer.ID);
            return new RemotePlayer(dto);
        } catch (Exception e) {
            onErrorCallback?.Invoke(e.Message);
        }
        return null;
    }
}
