﻿using System;
using System.Threading.Tasks;

public interface IPlayerRepository
{
    public Task<Player> CreatePlayer(string name, string password, Action<string> onErrorCallback);
    public Task<Player> LoginPlayer(string name, string password, Action<string> onErrorCallback);

    public Task<Player> GetOpponent(Player currentPlayer, Action<string> onErrorCallback);
}