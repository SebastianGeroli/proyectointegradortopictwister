using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using NSubstitute;
public class TurnShould
{
    Round round;
    Player player;
    LocalTurn turn;
    [SetUp]
    public void Before()
    {
        round = Substitute.For<Round>();
        player = Substitute.For<Player>();
        turn = new LocalTurn(player);
    }

    [Test]
    public void Return_zero_as_FinishTime_when_finish_turn_is_called_with_a_negative_number()
    {
        //when
        turn.FinishTurn(-2f, null, round);

        //then
        Assert.AreEqual(0, turn.FinishTime);
    }

    [Test]
    public void Have_two_correct_answers_and_two_incorrect_answers_with__FinishTime_of_five_seconds()
    {
        //Given
        round.Letter = new Letter("a");
        round.Categories = new List<Category>() { new Category("Animal"), new Category("Objeto"), new Category("Musica"), new Category("Cine")};
        List<Word> words = new List<Word>() { new Word("aguila"), new Word("anillo"), new Word("elvis"), new Word("eragon") };

        //When
        turn.FinishTurn(5f, words, round);

        //Then
        Assert.AreEqual(5f, turn.FinishTime);
        Assert.AreEqual(2, turn.CorrectAnswers);

        Assert.IsTrue(turn.Answers[0].IsCorrect);
        Assert.IsTrue(turn.Answers[1].IsCorrect);

        Assert.IsFalse(turn.Answers[2].IsCorrect);
        Assert.IsFalse(turn.Answers[3].IsCorrect);
    }

    [Test]
    public void Return_false_on_Finished_if_FinishTurn_was_not_called()
    {
        //Then
        Assert.IsFalse(turn.Finished);
    }

    [Test]
    public void Return_true_on_Finished_if_FinishTurn_was_called()
    {

        //When
        turn.FinishTurn(0.3f, null, round);

        //Then
        Assert.IsTrue(turn.Finished);
    }

    [TestCase(10f)]
    [TestCase(0.50f)]
    public void If_FinishTurn_is_called_with_a_positive_number_it_should_return_same_number(float timeLeft)
    {
        //When
        turn.FinishTurn(timeLeft, null, round);

        //Then
        Assert.AreEqual(timeLeft, turn.FinishTime);
    }
}
