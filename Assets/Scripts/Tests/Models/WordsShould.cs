using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class WordsShould
{
    [Test]
    [Ignore("Cambios en la regla de negocio")]
    public void OnlyLettersAndSpaces() {
        //Given

        //When
        Word word = new Word("HOLA MUNDO   123");
        //Assert
        Assert.AreEqual("HOLA MUNDO ", word.Name);
    }

    [Test]
    public void InUppercase() {
        //Given

        //When
        Word word = new Word("hola mundo");
        //Assert
        Assert.AreEqual("HOLA MUNDO", word.Name);
    }

    [Ignore("Cambios en la regla de negocio")]
    [Test]
    public void RepalcedDiacritics() {
        //Given

        //When
        Word word = new Word("H�L� M�ND�");
        //Assert
        Assert.AreEqual("HOLA MUNDO", word.Name);
    }

    [Test]
    public void NoSpacesOnTheSides() {
        //Given

        //When
        Word word = new Word("  HOLA MUNDO  ");
        //Assert
        Assert.AreEqual("HOLA MUNDO", word.Name);
    }

}
