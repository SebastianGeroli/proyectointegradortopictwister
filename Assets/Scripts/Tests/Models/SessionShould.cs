using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;
using System;
using NSubstitute;
public class SessionShould
{

    Player player1;
    Player player2;
    Round[] rounds;

    LocalSession session;
    [SetUp]
    public void Before()
    {
        player1 = Substitute.For<Player>();
        player2 = Substitute.For<Player>();

        rounds = new Round[2];

        rounds[0] = Substitute.For<Round>();
        rounds[1] = Substitute.For<Round>();

        session = new LocalSession( rounds);
    }

    [Test]
    public void Return_false_on_property_Finished_when_any_of_rounds_have_not_finished()
    {
        //When
        session.Rounds[0].Finished.Returns(false);
        session.Rounds[1].Finished.Returns(false);

        //Then
        Assert.IsFalse(session.Finished);
    }

    [Test]
    public void Return_true_on_property_Finished_when_all_rounds_have_finished()
    {
        //When
        session.Rounds[0].Finished.Returns(true);
        session.Rounds[1].Finished.Returns(true);
        //Then
        Assert.IsTrue(session.Finished);
    }

    [Test]
    public void Return_round2_as_CurrentRound()
    {
        //When
        session.Rounds[0].Finished.Returns(true);
        session.Rounds[1].Finished.Returns(false);

        //Then
        Assert.AreEqual(session.Rounds[1], session.CurrentRound);
    }

    [Test]
    public void Change_each_round_which_player_starts_playings()
    {
        //When
        session = new LocalSession(player1, player2, 5);
        
        //Then
        Assert.AreEqual(player1, session.Rounds[0].CurrentPlayer);
        Assert.AreEqual(player2, session.Rounds[1].CurrentPlayer);
        Assert.AreEqual(player1, session.Rounds[2].CurrentPlayer);
        Assert.AreEqual(player2, session.Rounds[3].CurrentPlayer);
        Assert.AreEqual(player1, session.Rounds[4].CurrentPlayer);
    }
}
