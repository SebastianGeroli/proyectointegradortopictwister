using NSubstitute;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterShould 
{
    LetterManager letterMgr;
    [SetUp]
    public void Before() {
        ILetterRepository letterRepository = Substitute.For<ILetterRepository>();
        letterRepository.GetRandomLetter().Returns(new Letter("a"));
        letterMgr = new LetterManager(letterRepository);
    }

    [Test]
    public void Return_A_Letter_RandomLetter() {
        //When
        Letter letter=null;
        letterMgr.GetRandomLetter((l)=> { letter = l; });
        //Assert
        Assert.IsNotNull(letter);
        Assert.IsNotEmpty(letter.Char.ToString()) ;
    }

}
