using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotPlayerShould
{
    GameDictionary _dictionary = new GameDictionary();
    IWordValidator wordValidator;
    BotPlayer botPlayer;

    [SetUp]
    public void Before() {
        wordValidator = new WordValidator(_dictionary);
        botPlayer = new BotPlayer("Bot", _dictionary);
    }

    [Test]
    public void Has_A_Name() {
        //Given
        string botName = "Bot1";
        //When
        botPlayer = new BotPlayer(botName, _dictionary);
        //Then
        Assert.AreEqual(botName, botPlayer.Name);
    }

    [Test]
    public void Has_A_Success_Probability_GreaterOrEqual_Than_Default_Value() {
        //When
        botPlayer.SuccessProbability = 0;
        //Then
        Assert.GreaterOrEqual(botPlayer.SuccessProbability, BotPlayer.MinSuccessProbability);
    }

    [Test]
    public void Has_A_Success_Probability_LessOrEqual_Than_Max_Value() {
        //When
        botPlayer.SuccessProbability = 0;
        //Then
        Assert.LessOrEqual(botPlayer.SuccessProbability, BotPlayer.MaxSuccessProbability);
    }

    [Test]
    public void Answer_With_Correct_Word() {
        //Given
        var category = new Category("Animal");
        var letter = new Letter("e");
        //When
        string word = botPlayer.Answer(category, letter);
        bool result = wordValidator.ValidateWord(word, category, letter);
        //Then
        Assert.IsTrue(result);
    }

    [TestCase(0.30f, 60, true)]
    [TestCase(0.40f, 60, true)]
    [TestCase(0.60f, 60, true)]
    [TestCase(0.70f, 60, false)]
    public void Predict_Success_Before_Answer(float randomSuccessProbability, float successProbability, bool expected) {
        //Given
        botPlayer.SuccessProbability = successProbability / 100f;
        //When
        bool result = botPlayer.PredictSuccess(randomSuccessProbability);
        //Then
        Assert.AreEqual(expected, result);
    }

    [Test]
    public void Answer_With_Not_Empty_Word() {
        //When
        string word = botPlayer.GetRandomWord();
        //Then
        Assert.IsNotEmpty(word);
    }

}
