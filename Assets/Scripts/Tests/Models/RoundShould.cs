using System;
using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using NSubstitute;
using System.Collections.Generic;

public class RoundShould
{
    Player player1;
    Turn turn1;
    Player player2;
    Turn turn2;
    LocalRound round;

    [SetUp]
    public void Before() {
        player2 = Substitute.For<Player>();
        player1 = Substitute.For<Player>();
        turn1 = Substitute.For<Turn>();
        turn2 = Substitute.For<Turn>();
        round = new LocalRound(turn1, turn2);

    }

    [Test]
    public void Return_Finished_true_when_both_players_have_played() {
        //When both players played
        round.Turn1.Finished.Returns(true);
        round.Turn2.Finished.Returns(true);

        //Then
        Assert.IsTrue(round.Finished);
    }

    [Test]
    public void Return_Finished_false_when_any_of_the_players_has_not_played_yet() {
        //When both players played
        round.Turn1.Finished.Returns(true);
        round.Turn2.Finished.Returns(false);
        //Then
        Assert.False(round.Finished);
    }

    [Test]
    public void Return_player1_as_winner_with_same_amount_of_CorrectAnswers_and_it_has_better_time_also_the_round_is_finished() {
        //When both players played
        round.Turn1.Finished.Returns(true);
        round.Turn2.Finished.Returns(true);

        round.Turn1.FinishTime.Returns(0.3f);
        round.Turn2.FinishTime.Returns(0f);

        round.Turn1.CorrectAnswers.Returns(3);
        round.Turn2.CorrectAnswers.Returns(3);

        //Then
        Assert.AreEqual(1, round.Winner.Length);
        Assert.Contains(round.Turn1.Player, round.Winner);
    }

    [Test]
    public void Return_player2_as_winner_with_same_amount_of_CorrectAnswers_and_it_has_better_time_also_the_round_is_finished() {

        //When both players played
        round.Turn1.Finished.Returns(true);
        round.Turn2.Finished.Returns(true);

        round.Turn1.FinishTime.Returns(0f);
        round.Turn2.FinishTime.Returns(0.3f);

        round.Turn1.CorrectAnswers.Returns(3);
        round.Turn2.CorrectAnswers.Returns(3);

        //Then
        Assert.AreEqual(1, round.Winner.Length);
        Assert.Contains(round.Turn2.Player, round.Winner);
    }

    [Test]
    public void Return_both_players_when_is_a_draw_with_same_amount_of_CorrectAnswers_and_same_time_also_the_round_is_finished() {
        //When both players played
        round.Turn1.Finished.Returns(true);
        round.Turn2.Finished.Returns(true);

        round.Turn1.FinishTime.Returns(0.3f);
        round.Turn2.FinishTime.Returns(0.3f);

        round.Turn1.CorrectAnswers.Returns(3);
        round.Turn2.CorrectAnswers.Returns(3);

        //Then
        Assert.Contains(round.Turn1.Player, round.Winner);
        Assert.Contains(round.Turn2.Player, round.Winner);
    }

    [Test]
    public void Return_empty_array_when_the_round_is_not_finihed_yet() {
        round.Turn1.Finished.Returns(false);
        round.Turn2.Finished.Returns(true);

        //Then
        Assert.IsNotNull(round.Winner);
        Assert.AreEqual(0, round.Winner.Length);
    }

    [Test]
    public void Return_player1_as_winner_when_it_has_more_CorrectAnswers_and_the_round_is_finished() {

        //When both players played
        round.Turn1.Finished.Returns(true);
        round.Turn2.Finished.Returns(true);

        round.Turn1.FinishTime.Returns(0.3f);
        round.Turn2.FinishTime.Returns(0.3f);

        round.Turn1.CorrectAnswers.Returns(4);
        round.Turn2.CorrectAnswers.Returns(3);

        //Then
        Assert.AreEqual(1, round.Winner.Length);
        Assert.Contains(round.Turn1.Player, round.Winner);
    }

    [Test]
    public void Return_player2_as_winner_when_it_has_more_CorrectAnwers_and_the_round_is_finished() {

        //When both players played
        round.Turn1.Finished.Returns(true);
        round.Turn2.Finished.Returns(true);

        round.Turn1.FinishTime.Returns(0.3f);
        round.Turn2.FinishTime.Returns(0.3f);

        round.Turn1.CorrectAnswers.Returns(3);
        round.Turn2.CorrectAnswers.Returns(4);

        //Then
        Assert.AreEqual(1, round.Winner.Length);
        Assert.Contains(round.Turn2.Player, round.Winner);
    }

    [Test]
    public void Return_player1_as_currentPlayer_if_no_one_has_finished_the_turns() {

        //When
        round.Turn1.Finished.Returns(false);
        round.Turn2.Finished.Returns(false);

        //Then
        Assert.AreEqual(round.Turn1.Player, round.CurrentPlayer);
    }

    [Test]
    public void Return_player2_as_currentPlayer_if_player1_has_finished_his_turn() {

        //When
        round.Turn1.Finished.Returns(true);
        round.Turn2.Finished.Returns(false);

        //Then
        Assert.AreEqual(round.Turn2.Player, round.CurrentPlayer);
    }

    [Test]
    public void Return_null_if_both_players_have_finished_their_turn() {
        //When
        round.Turn1.Finished.Returns(true);
        round.Turn2.Finished.Returns(true);

        //Then
        Assert.AreEqual(null, round.CurrentPlayer);
    }

    [Test]
    public void Not_be_able_to_change_Letter_if_its_set() {
        //When
        round.Letter = new Letter('E');
        round.Letter = new Letter('A');

        //Then
        Assert.AreEqual("E", round.Letter.Char);
        Assert.AreNotEqual("A", round.Letter.Char);
    }

    [Test]
    public void Not_be_able_to_change_Categories_if_its_set() {
        //Given
        List<Category> categories = new List<Category>() { new Category("Animal") };
        List<Category> categories2 = new List<Category>() { new Category("Animal"), new Category("Objeto") };

        //When
        round.Categories = categories;
        round.Categories = categories2;

        //Then
        Assert.AreEqual(categories, round.Categories);
    }
}
