using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShould
{
    [TestCase("abcd", PlayerNameError.NO_ERROR)]
    [TestCase("abc", PlayerNameError.WRONG_NAME)]
    [TestCase("abcd9080", PlayerNameError.NO_ERROR)]
    [TestCase("abcdASX", PlayerNameError.NO_ERROR)]
    [TestCase("", PlayerNameError.WRONG_NAME)]
    [TestCase("a", PlayerNameError.WRONG_NAME)]
    [TestCase("12", PlayerNameError.WRONG_NAME)]
    public void Write_A_Name_With_At_Least_4_Letters(string name, PlayerNameError expected) {
        //Given
        //When
        PlayerNameError result = LocalPlayer.VerifyName(name);
        //Then
        Assert.AreEqual(expected, result);
    }
    [Test]
    public void Have_A_Unique_Identifier() {
        //Given
        string namePlayer1 = "Escorpion";
        string namePlayer2 = "SubZero";

        //When
        LocalPlayer player1 = new LocalPlayer(namePlayer1);
        LocalPlayer player2 = new LocalPlayer(namePlayer2);
        //Then
        Assert.AreNotEqual(player1.ID,player2.ID);
    }
}
