using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class CategoryShould
{
    CategoryManager categoryManager;
    [SetUp]
    public void Before() {
        categoryManager = new CategoryManager(new LocalCategoryRepository());
    }

    [Test]

    public void ReturnThreeCategoriesNames() {
        //given         
        int value = 3;
        List<Category> cateogries = new List<Category>();
        //when
        categoryManager.GetRandomCategories(value, (cats) => { cateogries = cats; });

        //assert
        Assert.AreEqual(value, cateogries.Count);
    }

    [Test]

    public void NotRepeatCategoryNamesIfAmountOfCategoriesIsGreaterThanAmountAsk() {
        //Given
        List<Category> categories = new List<Category>();
        categoryManager.GetRandomCategories(5, (cats) => { 
            categories = cats; 
        });
        //When
        List<string> categoriesName = CategoryManager.GetCategoriesNames(categories);
        //Assert
        Assert.AreNotEqual(categoriesName[0], categoriesName[1]);
        Assert.AreNotEqual(categoriesName[0], categoriesName[2]);
        Assert.AreNotEqual(categoriesName[1], categoriesName[2]);
        Assert.AreNotEqual(categoriesName[0], categoriesName[3]);
        Assert.AreNotEqual(categoriesName[1], categoriesName[3]);
        Assert.AreNotEqual(categoriesName[2], categoriesName[3]);
        Assert.AreNotEqual(categoriesName[0], categoriesName[4]);
        Assert.AreNotEqual(categoriesName[1], categoriesName[4]);
        Assert.AreNotEqual(categoriesName[2], categoriesName[4]);
        Assert.AreNotEqual(categoriesName[3], categoriesName[4]);
    }

    [Test]

    public void RepeatCategoryNamesIfAmountOfCategoriesIsLowerThanAmountAsk() {
        //Given
        List<Category> categories = new List<Category>();
        List<Category> Allcategories = new List<Category>();
        categoryManager.GetCategories((cats) => { Allcategories = cats; });
        int amount = Allcategories.Count;
        //When
        categoryManager.GetRandomCategories(Allcategories.Count + 1, (cats) => { categories = cats; });
        List<string> categoriesName = CategoryManager.GetCategoriesNames(categories);
        List<string> categoriesAuxiliar = categoriesName;
        categoriesAuxiliar.RemoveAt(categoriesAuxiliar.Count - 1);

        //Assert
        Assert.IsTrue(categoriesAuxiliar.Contains(categoriesName[categoriesName.Count - 1]));
    }

    [TestCase(0)]
    [TestCase(-1)]

    public void ReturnEmptyList(int amountOfCategories) {
        //When
        List<Category> categories = new List<Category>() { new Category("1") };
        categoryManager.GetRandomCategories(amountOfCategories, (cats) => { categories = cats; });
        //Assert
        Assert.AreEqual(0, categories.Count);
    }
}
