using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordValidatorShould
{
    //incorrecta: vac�a, mal escrita, no aplica
    //correcta: corresponde a categor�a y letra
    //neutra: corresponde a categor�a y letra y tiene un �nico error ortogr�fico

    Category animal = new Category("Animal");
    Category objeto = new Category("Objeto");
    Category fruta = new Category("Fruta");
    WordValidator wordValidator;

    [SetUp]

    public void SetUp() {

        List<Word> wordList = new List<Word>();
        List<Category> categoryListOso = new List<Category>();
        List<Category> categoryListMesa = new List<Category>();
        List<Category> categoryListBanana = new List<Category>();

        categoryListOso.Add(animal);
        categoryListMesa.Add(objeto);
        categoryListBanana.Add(fruta);

        Word oso = new Word("oso", categoryListOso);
        Word mesa = new Word("mesa", categoryListMesa);
        Word banana = new Word("banana", categoryListBanana);

        wordList.Add(oso);
        wordList.Add(mesa);
        wordList.Add(banana);

        GameDictionary gameDictionary = new GameDictionary(wordList);
        wordValidator = new WordValidator(gameDictionary);
    }

    [Test]

    public void Return_True_If_Word_Is_Correct() {
        //given
        bool result;

        //when
        result = wordValidator.ValidateWord("oso", animal, new Letter("o"));

        //then
        Assert.IsTrue(result);
    }

    [Test]

    public void Return_False_If_Word_Is_Incorrect_Because_Is_Not_In_Category() {
        //given
        bool result;

        //when
        result = wordValidator.ValidateWord("oso", fruta, new Letter("o"));

        //then
        Assert.IsFalse(result);
    }

    [Test]

    public void Return_False_If_Word_Is_Incorrect_Because_Doesnt_Start_With_Correct_Letter() {
        //given
        bool result;

        //when
        result = wordValidator.ValidateWord("oso", animal, new Letter("a"));

        //then
        Assert.IsFalse(result);
    }

    [TestCase("oso")]
    [TestCase("oSo")]
    [TestCase("osO")]
    [TestCase("OSO")]
    [TestCase("OSo")]
    [TestCase("Oso")]
    public void Not_Be_Case_Sensitive_When_Checking_Category(string word) {
        //given
        bool result;

        //when
        result = wordValidator.ValidateWord(word, animal, new Letter("o"));

        //then
        Assert.IsTrue(result);
    }

    [Test]
    public void Return_False_If_The_Word_Is_Not_In_Dictionary() {
        //Given
        bool result;
        //When
        result = wordValidator.ValidateWord("leon", animal, new Letter("o"));
        //Then
        Assert.IsFalse(result);
    }

}
