using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotManagerShould 
{
    [Test]

    public void CreateBotAsAPlayer()
    {
        //Given
        //When
        Player player = BotManager.CreatePlayer();

        //then
        Assert.IsInstanceOf(typeof(BotPlayer),player);
    }

    [Test]

    public void CompleteBotTurn()
    {
        Player bot = BotManager.CreatePlayer();
        Player player = new LocalPlayer("Player1");
        LocalRound round = new LocalRound(player,bot);
        LocalTurn turn = new LocalTurn(bot);

        BotManager.CompleteTurn(round, turn, (BotPlayer)bot);

        Assert.AreEqual(round.Categories.Count, turn.Answers.Count);
    }

}
