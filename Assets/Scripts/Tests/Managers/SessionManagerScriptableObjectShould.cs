﻿using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;
using UnityEngine;

public class SessionManagerScriptableObjectShould
{

    //Saber de las sesiones activas en cual puedo jugar

    //No Poder jugar en una sesion que no me corrsponde todavia

    //Poder jugar en una sesion que me corresponde

    Player player1;
    SessionManagerScriptableObject sessionManager;

    [SetUp]
    public void Before() {
        player1 = Substitute.For<Player>();
        player1.ID.Returns("player1");
        sessionManager = ScriptableObject.CreateInstance<SessionManagerScriptableObject>();
        sessionManager.Login(player1);
    }

    [Test]
    public void Not_add_a_session_if_already_exists_in_the_list() {
        //Given
        var session1 = Substitute.For<Session>();

        //When
        sessionManager.AddSession(session1);
        sessionManager.AddSession(session1);

        //Then
        Assert.AreEqual(1, sessionManager.Sessions.Count);
    }

    [Test]
    public void Have_3_active_session_and_2_finished_sessions() {
        //Given
        var session1 = Substitute.For<Session>();
        var session2 = Substitute.For<Session>();
        var session3 = Substitute.For<Session>();
        var session4 = Substitute.For<Session>();
        var session5 = Substitute.For<Session>();
        sessionManager.AddSession(session1);
        sessionManager.AddSession(session2);
        sessionManager.AddSession(session3);
        sessionManager.AddSession(session4);
        sessionManager.AddSession(session5);

        session1.Finished.Returns(true);
        session2.Finished.Returns(true);
        session3.Finished.Returns(false);
        session4.Finished.Returns(false);
        session5.Finished.Returns(false);

        session1.CurrentRound.Returns(Substitute.For<Round>());
        session2.CurrentRound.Returns(Substitute.For<Round>());
        session3.CurrentRound.Returns(Substitute.For<Round>());
        session4.CurrentRound.Returns(Substitute.For<Round>());
        session5.CurrentRound.Returns(Substitute.For<Round>());

        session1.CurrentRound.CurrentPlayer.Returns(player1);
        session2.CurrentRound.CurrentPlayer.Returns(player1);
        session3.CurrentRound.CurrentPlayer.Returns(player1);
        session4.CurrentRound.CurrentPlayer.Returns(player1);
        session5.CurrentRound.CurrentPlayer.Returns(player1);
        //When
        List<Session> finishedSessions = sessionManager.FinishedSessions;

        List<ActiveSessionDTO> activeSessions = new List<ActiveSessionDTO>();
        sessionManager.GetActiveSessions((list)=> { activeSessions = list; },null);
        //Then
        Assert.AreEqual(finishedSessions[0], sessionManager.Sessions[0]);
        Assert.AreEqual(finishedSessions[1], sessionManager.Sessions[1]);
        Assert.AreEqual(activeSessions[0].sessionID, sessionManager.Sessions[2].ID);
        Assert.AreEqual(activeSessions[1].sessionID, sessionManager.Sessions[3].ID);
        Assert.AreEqual(activeSessions[2].sessionID, sessionManager.Sessions[4].ID);
    }

    [Test]
    public void GetActiveSessions_should_be_ordered_with_playable_sessions_first() {
        //Given
        var player2 = Substitute.For<Player>();
        player2.ID.Returns("player2");


        var session1 = Substitute.For<Session>();
        var session2 = Substitute.For<Session>();
        var session3 = Substitute.For<Session>();
        var session4 = Substitute.For<Session>();
        var session5 = Substitute.For<Session>();
        var session6 = Substitute.For<Session>();

        sessionManager.AddSession(session1);
        sessionManager.AddSession(session2);
        sessionManager.AddSession(session3);
        sessionManager.AddSession(session4);
        sessionManager.AddSession(session5);
        sessionManager.AddSession(session6);

        session1.CurrentRound.Returns(Substitute.For<Round>());
        session2.CurrentRound.Returns(Substitute.For<Round>());
        session3.CurrentRound.Returns(Substitute.For<Round>());
        session4.CurrentRound.Returns(Substitute.For<Round>());
        session5.CurrentRound.Returns(Substitute.For<Round>());
        session6.CurrentRound.Returns(Substitute.For<Round>());

        session1.CurrentRound.CurrentPlayer.Returns(player2);
        session2.CurrentRound.CurrentPlayer.Returns(player1);
        session3.CurrentRound.CurrentPlayer.Returns(player2);
        session4.CurrentRound.CurrentPlayer.Returns(player2);
        session5.CurrentRound.CurrentPlayer.Returns(player1);
        session6.CurrentRound.CurrentPlayer.Returns(player1);

        session1.GetOpponent(player2).Returns(player1);
        session2.GetOpponent(player1).Returns(player2);
        session3.GetOpponent(player2).Returns(player1);
        session4.GetOpponent(player2).Returns(player1);
        session5.GetOpponent(player1).Returns(player2);
        session6.GetOpponent(player1).Returns(player2);

        //When

        List<ActiveSessionDTO> availableSessions = new List<ActiveSessionDTO>();
        sessionManager.GetActiveSessions((list) => { availableSessions = list; }, null);

        //Then
        Assert.AreEqual(availableSessions[0].sessionID, sessionManager.Sessions[1].ID);
        Assert.AreEqual(availableSessions[1].sessionID, sessionManager.Sessions[4].ID);
        Assert.AreEqual(availableSessions[2].sessionID, sessionManager.Sessions[5].ID);
    }

    [Test]
    public void Not_modify_CurrentSession_if_the_CurrentPlayer_ID_does_not_match() {
        //Given
        var player2 = Substitute.For<Player>();
        player2.ID.Returns("player2");

        var session1 = Substitute.For<Session>();
        var session2 = Substitute.For<Session>();

        sessionManager.AddSession(session1);
        sessionManager.AddSession(session2);

        session1.CurrentRound.Returns(Substitute.For<Round>());
        session2.CurrentRound.Returns(Substitute.For<Round>());

        session1.CurrentRound.CurrentPlayer.Returns(player1);
        session2.CurrentRound.CurrentPlayer.Returns(player2);

        sessionManager.PlaySession(session1);
        //When
        sessionManager.PlaySession(session2);

        //Then
        Assert.AreEqual(session1, sessionManager.CurrentSession);
        Assert.AreNotEqual(session2, sessionManager.CurrentSession);
    }

    [Test]
    public void Modify_CurrentSession_if_the_CurrentPlayer_ID_matches() {
        //Given

        var session1 = Substitute.For<Session>();
        var session2 = Substitute.For<Session>();

        sessionManager.AddSession(session1);
        sessionManager.AddSession(session2);

        session1.CurrentRound.Returns(Substitute.For<Round>());
        session2.CurrentRound.Returns(Substitute.For<Round>());

        session1.CurrentRound.CurrentPlayer.Returns(player1);
        session2.CurrentRound.CurrentPlayer.Returns(player1);

        sessionManager.PlaySession(session1);
        //When
        sessionManager.PlaySession(session2);

        //Then
        Assert.AreEqual(session2, sessionManager.CurrentSession);
        Assert.AreNotEqual(session1, sessionManager.CurrentSession);
    }
}
