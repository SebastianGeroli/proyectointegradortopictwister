﻿using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;

[Category("Presenters")]
class CategoriesSelectorPresenterShould
{
    Round round;
    ICategoriesSelectorView view; 
    CategoriesSelectorPresenter presenter;
    Player player;
    ICategoryRepository categoryRepository;


    [SetUp]
    public void Before() {
        round = Substitute.For<Round>();
        view = Substitute.For<ICategoriesSelectorView>();

        player = Substitute.For<Player>();
        player.Name.Returns("TestPlayer");
        round.CurrentPlayer.Returns(player);

        categoryRepository = new LocalCategoryRepository();
        presenter = new CategoriesSelectorPresenter(view, round, categoryRepository);
    }

    [Test]
    public void InitializeTheView() {
        view.Received().Init(presenter);
    }

    [Test]
    public void HasThePlayer() {
        Assert.AreEqual(player.Name, presenter.PlayerName);
    }

    [Test]
    public void ShowPlayer() {
        view.Received().ShowPlayer(player.Name);
    }

    [Test]
    public void SelectCategoriesFromRandomListCategories() {
        List<string> categories = presenter.CategoriesNames;

        Assert.IsNotEmpty(categories);
    }

    [Test]
    public void ShowCategories() {
        view.Received().ShowCategories(presenter.CategoriesNames);
    }

    [Test]
    public void SaveTheCategoriesInTheSession() {
        presenter.GetRandomCategories();
        presenter.ConfirmCategories();
        Assert.AreEqual(round.Categories, presenter.categories);
    }

}
