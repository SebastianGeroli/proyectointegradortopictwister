using System.Collections;
using System.Collections.Generic;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

[Category("Presenters")]
public class SessionsPresenterShould 
{
    private ISessionsView view;
    private ISessionManager sessionManager;
    private SessionsPresenter presenter;
    Player player;
    List<Session> sessions;
    List<ActiveSessionDTO> activeSessionDTOs;

    [SetUp]
    public void Before() {
        player = Substitute.For<Player>();
        player.Name.Returns("TestPlayer");

        view = Substitute.For<ISessionsView>();
        sessionManager = Substitute.For<ISessionManager>();
        sessionManager.LocalPlayer.Returns(player);

        sessions = new List<Session>();
        activeSessionDTOs = new List<ActiveSessionDTO>();
        presenter = new SessionsPresenter(view, sessionManager, 0);
    }

    [Test]
    public void InitializeView() {
        view.Received().Init(presenter);
    }

    [Test]
    public void ShowPlayerName() {
        view.Received().ShowPlayer(player.Name);
    }

    [Test]
    public void ShowActiveSessions() {
        presenter.OnActiveSessions(activeSessionDTOs);
        view.Received().ShowActiveSessions(presenter.ActiveSessions);
    }

    [Test]
    public void PlaySession() {
        Session session = Substitute.For<Session>();
        Round round = Substitute.For<Round>();
        round.CurrentPlayer.Returns(player);
        Turn turn1 = Substitute.For<Turn>();
        turn1.Player.Returns(player);
        Turn turn2 = Substitute.For<Turn>();
        Player player2 = Substitute.For<Player>();
        player2.Name.Returns("Player2");
        turn2.Player.Returns(player2);
        round.Turn1.Returns(turn1);
        round.Turn2.Returns(turn2);

        session.Rounds.Returns(new Round[] { round });
        session.CurrentRound.Returns(round);
        sessions.Add(session);

        ActiveSessionDTO activeSession = new ActiveSessionDTO(session, player);
        bool isCalled = false;
        presenter.OnActiveSessionSelected += (Session s)=>{ 
            isCalled = s == session; 
        };

        presenter.PlaySession(activeSession);
        presenter.OnSessionReceived(session);

        sessionManager.Received().PlaySession(session.ID, presenter.OnSessionReceived,presenter.OnErrorGetSession);
        Assert.IsTrue(isCalled) ;        
    }

    [Test]
    public void StartNewSession() {
        Player opponent = Substitute.For<Player>();
        opponent.Name.Returns("TestPlayer");
        
        presenter.StartNewSession();
        presenter.ShowOpponentName(opponent.Name);

        sessionManager.Received().StartNewSession(presenter.ShowOpponentName);

    }

}
