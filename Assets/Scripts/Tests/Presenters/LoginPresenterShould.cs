using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

[TestFixture]
[Category("Presenters")]
public class LoginPresenterShould
{
    LoginPresenter loginPresenter;
    ILoginView loginView;
    ISessionManager sessionManager;
    IPlayerRepository playerRepository;

    Player playerBefore;

    [SetUp]
    public void Before() {
        playerBefore = PlayerManager.CurrentPlayer;

        loginView = Substitute.For<ILoginView>();
        sessionManager = Substitute.For<ISessionManager>();
        playerRepository = Substitute.For<IPlayerRepository>();
        loginPresenter = new LoginPresenter(loginView, sessionManager, playerRepository);
    }

    [TearDown]
    public void After() {
        PlayerManager.CurrentPlayer = playerBefore;
    }

    [Test]
    public void LoginWithName() {
        //Given
        string name = "Jesus";
        string password = "Jesus";
        Player player = Substitute.For<Player>();
        player.Name.Returns(name.ToUpper());
        player.ID.Returns(name);
        playerRepository.LoginPlayer(name, password, loginPresenter.OnLoginError).Returns(player);
        //When
        loginPresenter.Login(name, password);
        //Then
        Assert.AreEqual(name.ToUpper(), PlayerManager.CurrentPlayer.Name);

    }

    [Test]
    public void ErrorOnWrongName() {
        //Given
        string name = "AS";
        string password = "";
        playerRepository.CreatePlayer(name, password, null).Returns(Substitute.For<Player>());
        //When
        loginPresenter.Login(name, password);
        loginPresenter.OnLoginError("Nombre debe tener al menos 4 (cuatro) caracteres.");
        //Then
        loginView.Received().ShowErrorMessage("Nombre debe tener al menos 4 (cuatro) caracteres.");

    }

    [Test]
    public void ShowErrorMessageEmptyPlayerName() {
        string name = " ";
        string password = "";
        playerRepository.CreatePlayer(name, password, null).Returns(Substitute.For<Player>());

        loginPresenter.Login(name, password);
        loginPresenter.OnLoginError("Nombre debe tener al menos 4 (cuatro) caracteres.");

        loginView.Received().ShowErrorMessage("Nombre debe tener al menos 4 (cuatro) caracteres.");
    }

    [Test]
    public void OnLogin_LoginOnSession() {
        string name = "Ramon";
        string password = "";

        var player = Substitute.For<Player>();
        player.ID.Returns("89d54402-1074-4753-9a5b-44c22dea7153");
        player.Name.Returns(name.ToUpper());
        player.Password.Returns(password);
        playerRepository.LoginPlayer(name, password, loginPresenter.OnLoginError).Returns(player);

        loginPresenter.Login(name, password);

        sessionManager.Received().Login(player);
    }


}
