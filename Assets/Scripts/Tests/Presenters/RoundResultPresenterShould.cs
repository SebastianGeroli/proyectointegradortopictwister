using NSubstitute;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Category("Presenters")]
public class RoundResultPresenterShould
{
    ISessionManager sessionManager;
    IRoundResultView view;
    RoundResultPresenter presenter;
    Round round;

    [SetUp]
    public void Before() {
        view = Substitute.For<IRoundResultView>();
        sessionManager = Substitute.For<ISessionManager>();
        presenter = new RoundResultPresenter(view, sessionManager);
        round = Substitute.For<Round>();
        Session session = Substitute.For<Session>();
        session.CurrentRound.Returns(round);
        sessionManager.CurrentSession.Returns(session);
    }

    [Test]
    public void InitializeView() {
        presenter.Init();

        view.Received().Init(presenter);
    }

    [Test]
    public void FoundRoundResult() {
        presenter.Init();

        sessionManager.Received().GetRoundResult(round, presenter.OnRoundResultReceived, presenter.OnError);
    }

    [Test]
    public void ShowTitle() {
        presenter.Init();

        view.Received().ShowTitle("Resultados");
    }

    [Test]
    public void ShowResult() {
        FillTurns(round);
        round.Letter = new Letter("a");

        var dto = new RoundResultDTO(round);
        
        presenter.Init();
        presenter.OnRoundResultReceived(dto);

        view.Received().ShowResult(dto);
    }

    private void FillTurns(Round round) {
        Turn turn = Substitute.For<Turn>();
        Player Player = Substitute.For<Player>();
        turn.Player.Returns(Player);

        round.Turn1.Returns(turn);

        turn = Substitute.For<Turn>();
        Player = Substitute.For<Player>();
        turn.Player.Returns(Player);

        round.Turn2.Returns(turn);
    }
}
