using System.Collections;
using System.Collections.Generic;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

[Category("Presenters")]
public class LetterSelectorPresenterShould
{
    LetterSelectorPresenter presenter;
    private Round round;
    private ILetterSelectorView view;
    private ILetterRepository letterRepository;
    private ISessionManager sessionManager;

    [SetUp]
    public void Before() {
        view = Substitute.For<ILetterSelectorView>();
        round = Substitute.For<Round>();
        letterRepository = Substitute.For<ILetterRepository>();
        letterRepository.GetRandomLetter().Returns(new Letter('a'));
        sessionManager = Substitute.For<ISessionManager>();
        Session session = Substitute.For<Session>();
        session.CurrentRound.Returns(round);
        sessionManager.CurrentSession.Returns(session);
        presenter = new LetterSelectorPresenter(view, sessionManager, letterRepository);
    }

    [Test]
    public void InitializeView() {
        view.Received().Init(presenter);
    }

    [Test]
    public void HasEmptyLetter() {
        Assert.IsEmpty(presenter.CurrentLetter);
    }

    [Test]
    public void SelectRandomLetters() {
        presenter.SelectRandomLetter();

        Assert.AreNotEqual(' ', presenter.CurrentLetter);
    }

    [Test]
    public void SaveCurrentLetterOnSelection() {
        Letter letter = new Letter('a');

        presenter.OnLetterReceived(letter);

        Assert.AreEqual(letter.Char, presenter.CurrentLetter);

    }

    [Test]
    public void SaveLetterInToSession() {
        Letter letter = new Letter('a');

        presenter.OnLetterReceived(letter);

        sessionManager.Received().SetLetterCurrentRound(letter, presenter.OnSavedLetter, presenter.OnError);
    }

    [Test]
    public void ShowSelectedLetter() {
        presenter.OnLetterReceived(new Letter('a'));

        view.Received().ShowLetter("a");
    }

}
