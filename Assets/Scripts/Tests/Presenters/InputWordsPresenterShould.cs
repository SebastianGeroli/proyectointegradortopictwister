using System;
using System.Collections.Generic;
using NUnit.Framework;
using NSubstitute;

public class InputWordsPresenterShould
{
    private IInputWordsView view;
    private ISessionManager sessionManager;
    private Round round;
    private InputWordsPresenter presenter;
    private Turn turn;
    private Player player;
    private ITimer timer;

    [SetUp]
    public void Before() {
        view = Substitute.For<IInputWordsView>();
        round = Substitute.For<Round>();
        timer = Substitute.For<ITimer>();

        player = Substitute.For<Player>();
        player.Name.Returns("player");

        turn = Substitute.For<Turn>();
        turn.Player.Returns(player);
        round.CurrentTurn.Returns(turn);

        round.Categories = new List<Category> { new Category("Animal"), new Category("Objeto"), new Category("Cine") };
        round.Letter = new Letter('D');

        sessionManager = Substitute.For<ISessionManager>();

        Session session = Substitute.For<Session>();
        session.CurrentRound.Returns(round);
        sessionManager.CurrentSession.Returns(session);

        presenter = new InputWordsPresenter(view, sessionManager, timer);
    }

    [Test]
    public void HasCategoriesNameList() {
        Assert.AreEqual(round.Categories.Count, presenter.Categories.Count);
    }

    [Test]
    public void HasWordsForEachCategory() {
        Assert.AreEqual(round.Categories.Count, presenter.Words.Count);
        for (int i = 0; i < round.Categories.Count; i++) {
            Assert.IsNotNull(presenter.Words[i]);
        }
    }

    [Test]
    public void ShowPlayerName() {
        view.Received().ShowPlayerName(player.Name);
    }

    [Test]
    public void ShowLetter() {
        view.Received().ShowLetter(round.Letter.Char);
    }

    [Test]
    public void StartTimerOnInit() {
        presenter.Init();

        timer.Received().StartCountDown(60f, presenter.OnTimeOver);
    }

    [Test]
    public void InitViewOnInit() {
        presenter.Init();

        view.Received().Init(presenter);
    }

    [Test]
    public void ReplacedWordOnFirstWordChanged() {
        string expectedWord = "HOME";

        presenter.OnWordChanged(expectedWord, 0);

        Assert.AreEqual(expectedWord, presenter.Words[0]);
    }

    [Test]
    public void ReplacedWordsOnThreeWordsChanged() {
        string expectedWord1 = "DOG";
        string expectedWord2 = "DUCK";
        string expectedWord3 = "DOLPHIN";

        presenter.OnWordChanged(expectedWord1, 0);
        presenter.OnWordChanged(expectedWord2, 1);
        presenter.OnWordChanged(expectedWord3, 2);

        Assert.AreEqual(expectedWord1, presenter.Words[0]);
        Assert.AreEqual(expectedWord2, presenter.Words[1]);
        Assert.AreEqual(expectedWord3, presenter.Words[2]);
    }

    [Test]
    public void DisableInputsOnFinishTurn() {
        presenter.FinishTurn();

        view.Received().DisableInputs();
    }

    [Test]
    public void ShowButtonShowResultsOnFinishTurn() {
        presenter.FinishTurn();

        view.Received().ChangeButtonText("Ver resultados");
    }

    [Test]
    public void StopCountDownOnFinishTurn() {
        presenter.FinishTurn();

        timer.Received().StopCountDown();
    }

    [Test]
    public void OnTimeOverFinishTurn() {
        presenter.OnTimeOver();

        view.Received().ShowTimeOverMessage(3);
    }

}
